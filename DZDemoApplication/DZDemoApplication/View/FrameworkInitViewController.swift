//
//  FrameworkInitViewController.swift
//  DZDemoApplication
//
//  Created by Vuk on 24.5.22..
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import os

#if NATIVE
import DZ_Collector_tvOS
#elseif BITMOVIN
import BitmovinPlayer
import DZ_Collector_iOS
//#elseif AKAMAI
//import DZCollectorTvOSAkamai
//import AmpCore
#endif

#if canImport(GoogleInteractiveMediaAds)
import GoogleInteractiveMediaAds
#endif

#if canImport(AmoAMI)
import AmpAMI
import DZCollector
#endif

private let subsystem = "io.datazoom.DZFramework"

struct DZDemoLog {
    static let demoapp = OSLog(subsystem: subsystem, category: "demoapp")
}

class FrameworkInitViewController: UIViewController {
    // configuration
    // dev
    static private let nativeConfigId: String = "53960136-b38e-4464-8b08-d37afc728af4"
    static private let bitmovinConfigId: String = "53960136-b38e-4464-8b08-d37afc728af4"
    static private let akamaiConfigId: String = "24df8d6e-0804-4a01-881d-694c75dc2f7b"
    
    // staging
//    static private let nativeConfigId: String = "4c3fdb28-5107-4d18-aa9b-ddbb3386f487"
//    static private let akamaiConfigId: String = "e2192471-4f18-40aa-b5c8-718095e6ace0"
    
    
    @IBOutlet private weak var buildVersion: UILabel!
    @IBOutlet private weak var releaseVersion: UILabel!
    @IBOutlet var playerName: UILabel!
    @IBOutlet weak var frameworkBuildVersion: UILabel!
    
    @IBOutlet private weak var environments: UISegmentedControl!
    @IBOutlet private weak var configurationIdTextField: UITextField!

    @IBOutlet private weak var initializeLibraryButton: UIButton!
    
    var libraryConfiguration : DZLibraryConfiguration = DZLibraryConfiguration()
    var sdkInitDoneFlag: Bool = false
    
    var environmentChoioce = 0 // staging
    var configurationId = ""
    var selectedEnvironmentUrl: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        releaseVersion.text = Bundle.main.releaseVersionNumberPretty
        buildVersion.text = Bundle.main.buildVersionNumberPretty
//        frameworkBuildVersion.text = "build \(DZBaseFramework.getVersion())"
        
        environments.selectedSegmentIndex = environmentChoioce
        
        #if NATIVE
        configurationId = FrameworkInitViewController.nativeConfigId
        DispatchQueue.main.async {
            self.configurationIdTextField.text = self.configurationId
            self.playerName.text = "native"
        }
        #elseif BITMOVIN
        configurationId = FrameworkInitViewController.bitmovinConfigId
        DispatchQueue.main.async {
            self.configurationIdTextField.text = self.configurationId
            self.playerName.text = "bitmovin"
        }
        #elseif AKAMAI
        configurationId = FrameworkInitViewController.akamaiConfigId
        
        DispatchQueue.main.async {
            self.configurationIdTextField.text = self.configurationId
            self.playerName.text = "akamai"
        }
        #else
        DispatchQueue.main.async {
            self.configurationIdTextField.text = ""
            self.playerName.text = ""
        }
        #endif
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        
        configurationIdTextField.isEnabled = true
        self.initializeLibraryButton.isEnabled = true
        
        DispatchQueue.main.async {
            self.configurationIdTextField.text = self.configurationId
        }
#if NATIVE
        let keys: [String] = Array<String>(DemoAppData.shared.players.keys)
        for key in keys {
            let controller = DemoAppData.shared.controllers[key]
            controller?.player?.pause()
        }
#elseif BITMOVIN
        let keys: [String] = Array<String>(DemoAppData.shared.bitmovinPlayers.keys)
        for key in keys {
            let player = DemoAppData.shared.bitmovinPlayers[key]
            player?.pause()
        }
#endif
    }

}


extension FrameworkInitViewController {
    @IBAction private func pressEnvironmentSegment(_ sender: UISegmentedControl) {
        let tag:Int = sender.selectedSegmentIndex
        
        environmentChoioce = tag
        selectedEnvironmentUrl = DZEnvironment(rawValue: tag)?.urlString
        libraryConfiguration.apiEndpoint = selectedEnvironmentUrl ?? ""
        if(selectedEnvironmentUrl != nil && selectedEnvironmentUrl != ""){
            self.initializeLibraryButton.isEnabled = true
        }
    }
    
    @IBAction private func initLibraryButtonPressed(sender: UIButton) {
        libraryConfiguration = DZLibraryConfiguration(configurationId, DZEnvironment(rawValue:environmentChoioce)?.urlString ?? "")
        
        initLibraryWithConfiguration(libraryConfiguration) { (success, error) in
            if success == true {
                DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//                    self.initializeLibraryButton.isEnabled = false
                    
//                    let alert = UIAlertController(title: "Initialization", message: "Initialization done", preferredStyle: .alert)
//                        alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
//                    }))
//                    self.present(alert, animated: true, completion: nil)
                    
                    DemoAppData.shared.selectedPlayerId = ""
                    DemoAppData.shared.controllers = [:]
                    DemoAppData.shared.customEventName = ""
                    DemoAppData.shared.customEventData = [:]
                    
                    #if NATIVE
                        DemoAppData.shared.players = [:]
                    #endif
                    
                    self.performSegue(withIdentifier: "MainViewSegue", sender: self)
                }
            }
            else {
                if let error = error {
                    os_log("Error %s", log: DZDemoLog.demoapp, type: .error, error.localizedDescription)
                    DispatchQueue.main.async {
                        let alert = UIAlertController(title: "Initialization error", message: "Configuration not found", preferredStyle: .alert)
                            alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: "Default action"), style: .default, handler: { _ in
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
            }
        }
    }
}


// Initialization
extension FrameworkInitViewController {
    func validateConfigId(configId:String) -> Bool {
        if configId.range(of: "[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}", options: .regularExpression, range: nil, locale: nil) != nil {
            return true
        }else {
            return false
        }
    }
    
    func initLibraryWithConfiguration(_ libConfig: DZLibraryConfiguration, completion: @escaping (Bool,Error?) -> Void){
#if NATIVE
        DemoAppData.shared.nativeFramework = DZNativeCollector()
        DemoAppData.shared.nativeFramework?.initialize(configurationId: libConfig.configurationId, url: libConfig.apiEndpoint, { success, error in
            if success == true {
                os_log("Init Library done", log: DZDemoLog.demoapp, type: .info)
                DemoAppData.shared.initData()
                completion(true, nil)
            }
            else {
                if let error = error {
                    os_log("Error %s", log: DZDemoLog.demoapp, type: .error, error.localizedDescription)
                }
                completion(false, error)
            }
        })

#elseif BITMOVIN
        let apiEndpoint = libConfig.apiEndpoint + "/beacon/v1/"
        DemoAppData.shared.bitmovinFramework = DZ_Collector(configId: libConfig.configurationId, url: apiEndpoint)
        
        DemoAppData.shared.bitmovinFramework?.start(completion: { success, error in
            if success == true {
                //os_log("Init Library done", log: DZDemoLog.demoapp, type: .info)
                DemoAppData.shared.initData()
                completion(true, nil)
                
            }
            else {
                if let error = error {
                    os_log("Error %s", log: DZDemoLog.demoapp, type: .error, error.localizedDescription)
                }
                completion(false, error)
            }
        })
        
#elseif AKAMAI
//        DZAkamaiCollector.shared.flagForAutomationOnly = false
//        DZAkamaiCollector.shared.fluxDataFlag = true
//
//        DZAkamaiCollector.shared.initCollector(configID: libConfig.configurationId, url: libConfig.apiEndpoint) { (success, error) in
//            if success == true {
//                DZAkamaiCollector.shared.initCustomMetadata(self.playerMetadata, self.sessionMetadata, self.customMetadata)
//                onCompletion(true, nil)
//            }
//            else {
//                logger?.debug("Error Init Library \(error)")
//                onCompletion(false, error)
//            }
//        }
#endif
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is PlayersViewController {
//            let vc = segue.destination as? MainViewController
        }
    }
}

extension FrameworkInitViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if let newConfigurationId = textField.text {
            if validateConfigId(configId: newConfigurationId) == true {
                self.libraryConfiguration.configurationId = newConfigurationId
                self.configurationIdTextField.text = newConfigurationId
            }
            else {
                self.configurationIdTextField.text = self.libraryConfiguration.configurationId
            }
        }
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        return true
    }
}
