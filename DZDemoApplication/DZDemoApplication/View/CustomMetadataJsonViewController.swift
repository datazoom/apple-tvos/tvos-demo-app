//
//  CustomMetadataJsonViewController.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 13.12.21..
//

import Foundation
import UIKit

class CustomMetadataJsonViewController: UIViewController {

    var customMetadataController: CustomMetadataViewController? = nil
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    @IBOutlet private weak var customMetadataTextField: UITextField!
    @IBOutlet private weak var customMetadataTextView: UITextView!
    
    var customMetadata: [String: Any]? = nil
    var mainTitle: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        mainTitleLabel.text = mainTitle
        
        let metadata: String? = customMetadata?.jsonString()
        customMetadataTextField.text = metadata ?? ""
        let formatedMetadata: String? = customMetadata?.jsonString(prettify: true)
        customMetadataTextView.text = formatedMetadata ?? ""
    }
        
    @IBAction private func updateButtonPressed(_ sender: Any) {
        let metadata = customMetadataTextView.text
        if let metadataDictionary = metadata?.toJSON() as? [String: Any] {
            customMetadataController?.onCustomMetadataChanged(metadata: metadataDictionary)
            self.dismiss(animated: true, completion: nil)
        }
    }
}

extension CustomMetadataJsonViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        let metadata = textField.text
        if let metadataDictionary = metadata?.toJSON() as? [String: Any] {
            if let formatedMetadata = metadataDictionary.jsonString(prettify: true) {
                customMetadataTextView.text = formatedMetadata
            }
        }
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
        
    }
}



