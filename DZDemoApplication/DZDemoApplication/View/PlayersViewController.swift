//
//  ViewController.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 27.11.21..
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import os

#if NATIVE
import DZ_Collector_tvOS
#elseif BITMOVIN
import BitmovinPlayer
import DZ_Collector_iOS
//#elseif AKAMAI
//import DZCollectorTvOSAkamai
//import AmpCore
#endif

#if canImport(GoogleInteractiveMediaAds)
import GoogleInteractiveMediaAds
#endif

#if canImport(AmoAMI)
import AmpAMI
import DZCollector
#endif


private let subsystem = "io.datazoom.DZFramework"

struct DZDemoPlayer {
    static let demoplayer = OSLog(subsystem: subsystem, category: "demoplayer")
}

class PlayersViewController: UIViewController {
    static private let ampLicense = "AwECs0Ug2tYDP9TBnw4JK84rqQhHpukHYS5EA4Xn96eecCGIT2WTtX0Ur541fqZa5sRfpkqKX2NoDvz04gwkDzutD9Jh3JaY179WOfXLFzneVHOXEUC+3XnwjrISax4+qlohAAkl39ZE+kTRiRkdWHAKeSfs5lUp6CFqiNK27pPCZbLbyI5/+V6Vmmusp1fXoE7lPkOm8JV52+t0aoIIfKeF3+WdozIaHnzynOjA8OrlIw=="

    static private let bitmovinLicense = "9d5e04d1-31b3-42fe-a326-b10b248e2ada"
    
    @IBOutlet private weak var videoTypeButton: UIButton!
    @IBOutlet private weak var videoTypeLabel: UILabel!
    
    @IBOutlet private weak var adTypeButton: UIButton!
    @IBOutlet private weak var adTypeLabel: UILabel!
    
    @IBOutlet private weak var setPlayerMetadataButton: UIButton!
    @IBOutlet private weak var setSessionMetadataButton: UIButton!
    
    @IBOutlet private weak var resetPlayerMetadataButton: UIButton!
    @IBOutlet private weak var resetSessionMetadataButton: UIButton!
    
    @IBOutlet weak var createCustomEventButton: UIButton!
    @IBOutlet weak var sendCustomEventButton: UIButton!
    
    @IBOutlet private weak var playButton: UIButton!
    @IBOutlet private weak var playPlayerButton: UIButton!
    @IBOutlet private weak var playControllerButton: UIButton!
    
    @IBOutlet weak var removePlayerButton: UIButton!
    @IBOutlet weak var fullScreenButton: UIButton!
    
    @IBOutlet weak var stopCollectingDataButton: UIButton!
    @IBOutlet weak var startCollectingDataButton: UIButton!
    
    @IBOutlet weak var playerContainerView: UIView!
    
    @IBOutlet private weak var playersTableView : UITableView!
    
    var selectedVideoType: VideoType = VideoType.defaultType
    var selectedAdType: AdType = AdType.defaultType
    var selectedVideoUrl: String?
    var selectedAdUrl: String?

    var customMetadataSelector: Int = 0
        
    override func viewDidLoad() {
        super.viewDidLoad()
                
        selectedVideoUrl = selectedVideoType.url
        videoTypeLabel.text = selectedVideoType.title
        
        selectedAdUrl = selectedAdType.url
        adTypeLabel.text = selectedAdType.title
        
        enableControlls(flag: false)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
        enableControlls(flag: enableFlag)
    }
    
    private func addChildViewController(child: UIViewController) {
        self.addChild(child)
        self.playerContainerView.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.playerContainerView.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.playerContainerView.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.playerContainerView.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.playerContainerView.heightAnchor)
        ])
        
        child.didMove(toParent: self)
    }
    
    private func removeChildViewController(child: UIViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
    }
    
    // AVPlayer
    private func addChildViewController(child: AVPlayerViewController) {
        self.addChild(child)
        self.playerContainerView.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.playerContainerView.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.playerContainerView.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.playerContainerView.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.playerContainerView.heightAnchor)
        ])
        
        child.didMove(toParent: self)
    }
    
    private func removeChildViewController(child: AVPlayerViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
        child.view.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func ateachChildViewController(child: AVPlayerViewController) {
        self.playerContainerView.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.playerContainerView.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.playerContainerView.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.playerContainerView.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.playerContainerView.heightAnchor)
        ])
        
        child.didMove(toParent: self)
    }
    
    private func deteachChildViewController(child: AVPlayerViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.view.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func switchChildViewController(current: AVPlayerViewController?, next: AVPlayerViewController?) {
        if let ct = current {
            deteachChildViewController(child: ct)
        }
        if let nt = next {
            ateachChildViewController(child: nt)
        }
    }
    
    //Bitmovin
    private func addChildUIViewController(child: UIViewController) {
        self.addChild(child)
        self.playerContainerView.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.playerContainerView.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.playerContainerView.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.playerContainerView.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.playerContainerView.heightAnchor)
        ])
        
        child.didMove(toParent: self)
    }
    
    private func removeChildUIViewController(child: UIViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.removeFromParent()
        child.view.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func ateachChildUIViewController(child: UIViewController) {
        self.playerContainerView.addSubview(child.view)
        
        child.view.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            child.view.centerYAnchor.constraint(equalTo: self.playerContainerView.centerYAnchor),
            child.view.centerXAnchor.constraint(equalTo: self.playerContainerView.centerXAnchor),
            child.view.widthAnchor.constraint(equalTo: self.playerContainerView.widthAnchor),
            child.view.heightAnchor.constraint(equalTo: self.playerContainerView.heightAnchor)
        ])
        
        child.didMove(toParent: self)
    }
    
    private func deteachChildUIViewController(child: UIViewController) {
        child.willMove(toParent: nil)
        child.view.removeFromSuperview()
        child.view.translatesAutoresizingMaskIntoConstraints = true
    }
    
    private func switchChildViewController(current: UIViewController?, next: UIViewController?) {
        if let ct = current {
            deteachChildUIViewController(child: ct)
        }
        if let nt = next {
            ateachChildUIViewController(child: nt)
        }
    }
    
    
    func enableControlls(flag: Bool){
        DispatchQueue.main.async {
            self.setPlayerMetadataButton.isEnabled = flag
            self.resetPlayerMetadataButton.isEnabled = flag
            
            self.stopCollectingDataButton.isEnabled = flag
            self.startCollectingDataButton.isEnabled = flag
            
            self.removePlayerButton.isEnabled = flag
            self.fullScreenButton.isEnabled = flag
        }
    }
}

extension PlayersViewController {
    func onVideoTypeChanged(type: VideoType) {
        self.selectedVideoType = type
        self.selectedVideoUrl = type.url
        self.videoTypeLabel.text = type.title
    }
    
    func onAdTypeChanged(type: AdType) {
        self.selectedAdType = type
        self.selectedAdUrl = type.url
        self.adTypeLabel.text = type.title
    }
    
    func onCustomMetadataChanged(metadata: [String:Any]?) {
        if customMetadataSelector == 1 {
            guard let selected = DemoAppData.shared.selectedPlayerId else {
                return
            }
#if NATIVE
            DemoAppData.shared.nativeFramework?.setPlayerCustomMetadata(playerContext: selected, metadata)
#elseif BITMOVIN
            DemoAppData.shared.bitmovinFramework?.set(customMetadata: metadata!, playerContext: selected)
#endif
        }
        else if customMetadataSelector == 2 {
#if NATIVE
            DemoAppData.shared.nativeFramework?.setSessionCustomMetadata(metadata)
#elseif BITMOVIN
            DemoAppData.shared.bitmovinFramework?.set(customMetadata: metadata)
#endif
        }
    }
        
    func onCustomEventChanged(eventName: String?, eventData: [String: Any]?){
        DemoAppData.shared.customEventName = eventName ?? ""
        DemoAppData.shared.customEventData = eventData ?? [:]
    }
}

//
extension PlayersViewController {
    @IBAction private func videoTypeButtonPressed(sender: UIButton) {
        performSegue(withIdentifier: "ChooseVideoTypeSegue", sender: nil)
    }

    @IBAction private func adTypeButtonPressed(sender: UIButton) {
        performSegue(withIdentifier: "ChooseAdTypeSegue", sender: nil)
    }

    
    
    @IBAction private func setSessionCustomMetadataButtonPressed(sender: UIButton) {
        customMetadataSelector = 2
        performSegue(withIdentifier: "EnterCustomMetadataSeque", sender: nil)
    }

    @IBAction private func resetSessionCustomMetadataButtonPressed(sender: UIButton) {
#if NATIVE
        DemoAppData.shared.nativeFramework?.setSessionCustomMetadata([:])
#elseif BITMOVIN
        DemoAppData.shared.bitmovinFramework?.clearMetadata()
#endif
    }
    
    
    @IBAction private func setPlayerCustomMetadataButtonPressed(sender: UIButton) {
        customMetadataSelector = 1
        performSegue(withIdentifier: "EnterCustomMetadataSeque", sender: nil)
    }
    
    @IBAction private func resetPlayerCustomMetadataButtonPressed(sender: UIButton) {
        guard let selected = DemoAppData.shared.selectedPlayerId else {
            return
        }
#if NATIVE
        DemoAppData.shared.nativeFramework?.setPlayerCustomMetadata(playerContext: selected, [:])
#elseif BITMOVIN
        DemoAppData.shared.bitmovinFramework?.clearMetadata(playerContext: selected)
#endif
    }
    
    
    
    @IBAction private func createCustomEventButtonPressed(sender: UIButton) {
        performSegue(withIdentifier: "EnterCustomEventSeque", sender: nil)
    }
    
    @IBAction private func sendCustomEventButtonPressed(sender: UIButton) {
#if NATIVE
        let name = DemoAppData.shared.customEventName
        let data = DemoAppData.shared.customEventData
        
        DemoAppData.shared.nativeFramework?.sendCustomEvent(name: name, metadata: data)
//#elseif AKAMAI
//        DZAkamaiCollector.shared.customEvent(eventName: customEventName, metadata: customEventData)
#endif
    }
    
    
    @IBAction private func playButtonPressed(sender: UIButton) {
        guard let videoUrl = selectedVideoUrl else {
            return
        }
        
#if NATIVE
        DemoAppData.shared.nativeFramework?.initPlayer(url: videoUrl, { playerId, player, error in
            if let playerId = playerId, let player = player {
                DemoAppData.shared.players[playerId] = player
//                DemoAppData.shared.selectedPlayerId = playerId
                self.playersTableView.reloadData()
                
                let playerViewController = NativePlayerViewController()
                playerViewController.showsPlaybackControls = true
                playerViewController.requiresLinearPlayback = true
                playerViewController.player = player
                
                DemoAppData.shared.controllers[playerId] = playerViewController
                self.addChildViewController(child: playerViewController)
                
                playerViewController.player?.play()
                
                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
            }
        })
#elseif BITMOVIN
        guard let url = NSURL(string: videoUrl) as? URL else { return }
        
        let playerConfig = PlayerConfig()
        playerConfig.key = PlayersViewController.bitmovinLicense
        
        let player = PlayerFactory.create(playerConfig: playerConfig)
                
        if let playerId = DemoAppData.shared.bitmovinFramework?.startRecordingEvents(for: player) {
            guard let sourceConfig = SourceConfig(url: url) else { return }
            
            DemoAppData.shared.bitmovinPlayers[playerId] = player
//            DemoAppData.shared.selectedPlayerId = playerId
            self.playersTableView.reloadData()
        
            let playerViewController = BitmovinPlayerViewController()
            
//            let playerView = PlayerView(player: player, frame: .zero)
            let playerView = PlayerView(player: player, frame: playerContainerView.bounds)
            
            playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            playerView.frame = playerViewController.view.bounds

            playerViewController.view.addSubview(playerView)
            playerViewController.view.bringSubviewToFront(playerView)
            
            DemoAppData.shared.controllers[playerId] = playerViewController
            self.addChildViewController(child: playerViewController)
            
            player.load(sourceConfig: sourceConfig)
            player.play()
        }
        
        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
        self.enableControlls(flag: enableFlag)
#endif
    }
    
    @IBAction private func playPlayerButtonPressed(sender: UIButton) {
        guard let videoUrl = selectedVideoUrl else { return }
        guard let url = NSURL(string: videoUrl) as? URL else { return }
        
#if NATIVE
        let player = AVPlayer(url: url)
        
        DemoAppData.shared.nativeFramework?.initPlayer(player: player, { playerId, error in
            if let playerId = playerId {
                DemoAppData.shared.players[playerId] = player
//                DemoAppData.shared.selectedPlayerId = playerId
                self.playersTableView.reloadData()
                
                let playerViewController = NativePlayerViewController()
                playerViewController.showsPlaybackControls = true
                playerViewController.requiresLinearPlayback = true
                playerViewController.player = player
                
                DemoAppData.shared.controllers[playerId] = playerViewController
                
                self.addChildViewController(child: playerViewController)
                
                playerViewController.player?.play()
                
                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
            }
        })
#elseif BITMOVIN
        // Or with a explicit PlayerConfig
        let playerConfig = PlayerConfig()
        playerConfig.key = PlayersViewController.bitmovinLicense
        
        let player = PlayerFactory.create(playerConfig: playerConfig)
                
        if let playerId = DemoAppData.shared.bitmovinFramework?.startRecordingEvents(for: player) {
            guard let sourceConfig = SourceConfig(url: url) else { return }
            
            DemoAppData.shared.bitmovinPlayers[playerId] = player
            DemoAppData.shared.selectedPlayerId = playerId
            self.playersTableView.reloadData()
        
            let playerViewController = BitmovinPlayerViewController()
            
//            let playerView = PlayerView(player: player, frame: .zero)
            let playerView = PlayerView(player: player, frame: playerContainerView.bounds)
            
            playerView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
            playerView.frame = playerViewController.view.bounds

            playerViewController.view.addSubview(playerView)
            playerViewController.view.bringSubviewToFront(playerView)
            
            DemoAppData.shared.controllers[playerId] = playerViewController
            self.addChildViewController(child: playerViewController)
            
            player.load(sourceConfig: sourceConfig)
            player.play()
        }
        
        let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
        self.enableControlls(flag: enableFlag)
#endif
    }
    
    @IBAction private func stopCollectingDataButtonPressed(sender: UIButton) {
        if let selected = DemoAppData.shared.selectedPlayerId {
#if NATIVE
            DemoAppData.shared.nativeFramework?.stopCollectingData(playerContext: selected)
#elseif BITMOVIN
            DemoAppData.shared.bitmovinFramework?.stopRecordingEvents(playerId: selected)
#endif
        }
    }

    @IBAction private func startCollectingDataButtonPressed(sender: UIButton) {
        if let selected = DemoAppData.shared.selectedPlayerId {
#if NATIVE
            DemoAppData.shared.nativeFramework?.startCollectingData(playerContext: selected)
#elseif BITMOVIN
            if let bPlayer = DemoAppData.shared.bitmovinPlayers[selected] {
                let _ = DemoAppData.shared.bitmovinFramework?.startRecordingEvents(for: bPlayer)
            }
#endif
            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
            enableControlls(flag: enableFlag)
        }
    }
    
    
    @IBAction private func removePlayerButtonPressed(sender: UIButton) {
        if let selected = DemoAppData.shared.selectedPlayerId {
#if NATIVE
            DemoAppData.shared.nativeFramework?.deinitPlayer(playerContext: selected, { success, error in
                let controller = DemoAppData.shared.controllers[selected]
                controller?.player?.pause()
                controller?.player = nil
                DemoAppData.shared.controllers[selected] = nil
                DemoAppData.shared.players[selected] = nil
                DemoAppData.shared.selectedPlayerId = ""
                self.playersTableView.reloadData()
                
                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
            })
#elseif BITMOVIN
            DemoAppData.shared.bitmovinFramework?.stopRecordingEvents(playerId: selected)
            
            let controller = DemoAppData.shared.controllers[selected]
            DemoAppData.shared.controllers[selected] = nil
            DemoAppData.shared.bitmovinPlayers[selected] = nil
            DemoAppData.shared.selectedPlayerId = ""
            self.playersTableView.reloadData()
            
            let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
            self.enableControlls(flag: enableFlag)
#endif
        }
    }
    
    
    @IBAction private func fullScreenButtonPressed(sender: UIButton) {
        if let selected = DemoAppData.shared.selectedPlayerId {
            if let controller = DemoAppData.shared.controllers[selected] {
#if NATIVE
                DispatchQueue.main.async() {
                    self.removeChildViewController(child: controller)
                    
                    controller.modalPresentationStyle = .fullScreen
                    self.present(controller, animated: true) {
                        DemoAppData.shared.fullscreenFlag = true
                        controller.fullScreenFlag = true
                    }
                }
#elseif BITMOVIN
                DispatchQueue.main.async() {
                    self.removeChildUIViewController(child: controller)
                    
                    controller.modalPresentationStyle = .fullScreen
                    self.present(controller, animated: true) {
                        DemoAppData.shared.fullscreenFlag = true
                        controller.fullScreenFlag = true
                    }
                }
#endif
            }
        }
    }
    
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is VideoTypeViewController {
            let vc = segue.destination as? VideoTypeViewController
            vc?.selectedVideoType = selectedVideoType
            vc?.playersController = self
        }
        else if segue.destination is AdTypeViewController {
            let vc = segue.destination as? AdTypeViewController
            vc?.selectedAdType = selectedAdType
            vc?.playersController = self
        }
        else if segue.destination is CustomMetadataViewController {
            let vc = segue.destination as? CustomMetadataViewController
            if customMetadataSelector == 1 {
                vc?.mainTitle = "Player Metadata"
                if let selected = DemoAppData.shared.selectedPlayerId {
#if NATIVE
                    vc?.customData = DemoAppData.shared.nativeFramework?.getPlayerCustomMetadata(playerContext:selected) ?? [:]
#elseif BITMOVIN

#endif
                }
            }
            else if customMetadataSelector == 2 {
                vc?.mainTitle = "Session Metadata"
#if NATIVE
                vc?.customData = DemoAppData.shared.nativeFramework?.getSessionCustomMetadata() ?? [:]
#elseif BITMOVIN

#endif
            }
            vc?.playersController = self
        }
        else if segue.destination is CustomEventViewController {
            let vc = segue.destination as? CustomEventViewController
            vc?.customEventName = DemoAppData.shared.customEventName
            vc?.customEventData = DemoAppData.shared.customEventData
            vc?.playersController = self
        }
    }
}
        
extension PlayersViewController {
    func initMetadata(){
#if NATIVE
//        DemoAppData.shared.nativeFramework.initCustomMetadata(playerMetadata, sessionMetadata)
//#elseif AKAMAI
//        DemoAppData.shared.nativeFramework.initCustomMetadata(playerMetadata, sessionMetadata)
#endif
    }
}


extension PlayersViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
#if NATIVE
        return DemoAppData.shared.players.keys.count
#elseif BITMOVIN
        return DemoAppData.shared.bitmovinPlayers.keys.count
#endif
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRowIdentifier = "PlayersCellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellRowIdentifier)
#if NATIVE
        let keys: [String] = Array<String>(DemoAppData.shared.players.keys)
#elseif BITMOVIN
        let keys: [String] = Array<String>(DemoAppData.shared.bitmovinPlayers.keys)
#endif
        let key: String = keys[indexPath.row]
        cell?.textLabel?.text = key
        
        if DemoAppData.shared.selectedPlayerId == key {
           cell?.accessoryType = .checkmark
        }
        else {
            cell?.accessoryType = .none
        }
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
#if NATIVE
        let keys: [String] = Array<String>(DemoAppData.shared.players.keys)
#elseif BITMOVIN
        let keys: [String] = Array<String>(DemoAppData.shared.bitmovinPlayers.keys)
#endif
        let key: String = keys[indexPath.row]
        
//        let prevController = DemoAppData.shared.controllers[DemoAppData.shared.selectedPlayerId!]
        
        if key == DemoAppData.shared.selectedPlayerId! {
            DemoAppData.shared.selectedPlayerId = ""
            
            DispatchQueue.main.async() {
                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
                
                self.playersTableView.reloadData()
            }
        }
        else {
            let current = DemoAppData.shared.controllers[DemoAppData.shared.selectedPlayerId!]
            let next = DemoAppData.shared.controllers[key]
            
#if NATIVE
            let selectPlayer = DemoAppData.shared.players[key]

            DispatchQueue.main.async() {
//                self.playerViewController?.player = selectPlayer
                DemoAppData.shared.selectedPlayerId = key
                
                self.switchChildViewController(current: current,next: next)
                
                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
                
                self.playersTableView.reloadData()
            }
#elseif BITMOVIN
            let selectPlayer = DemoAppData.shared.bitmovinPlayers[key]
            
            DispatchQueue.main.async() {
                DemoAppData.shared.selectedPlayerId = key
                
                self.switchChildViewController(current: current,next: next)
                
                let enableFlag = DemoAppData.shared.selectedPlayerId != nil && DemoAppData.shared.selectedPlayerId != ""
                self.enableControlls(flag: enableFlag)
                
                self.playersTableView.reloadData()
            }
#endif
        }
    }
        
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}

