//
//  AdTypeViewController.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 27.11.21..
//

import Foundation
import UIKit

class AdTypeViewController: UIViewController {

    let adTypes: [AdType] = AdType.allCases
    var playersController: PlayersViewController? = nil
    var selectedAdType : AdType = AdType.defaultType
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension AdTypeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return adTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRowIdentifier = "AdTypeCellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellRowIdentifier)
        let adType = adTypes[indexPath.row]
        cell?.textLabel?.text = adType.title
        
        if selectedAdType == adType {
           cell?.accessoryType = .checkmark
        }
        else {
            cell?.accessoryType = .none
        }
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedAdType = adTypes[indexPath.row]
        playersController?.onAdTypeChanged(type: selectedAdType)
        self.dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}

