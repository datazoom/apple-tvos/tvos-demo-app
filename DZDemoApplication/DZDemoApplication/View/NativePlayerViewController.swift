//
//  NativePlayerViewController.swift
//  DZDemoApplication
//
//  Created by Ugljesha on 1.8.22..
//

import Foundation
import UIKit
import AVKit

class NativePlayerViewController: AVPlayerViewController {
    var fullScreenFlag: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        if fullScreenFlag {
            fullScreenFlag = false
            DemoAppData.shared.fullscreenFlag = false
        }
    }
}
