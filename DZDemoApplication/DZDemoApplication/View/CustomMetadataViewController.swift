//
//  CustomMetadataViewControllerFile.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 11.2.22..
//

import Foundation
import UIKit

class CustomMetadataViewController: UIViewController {

    var playersController: PlayersViewController? = nil
    var customEventController: CustomEventViewController? = nil
    
    @IBOutlet weak var mainTitleLabel: UILabel!
    
    @IBOutlet weak var metadataTable: UITableView!
    
    @IBOutlet weak var editJsonButton: UIButton!
    @IBOutlet weak var clearButton: UIButton!
    @IBOutlet weak var addStringButton: UIButton!
    @IBOutlet weak var addNumberButton: UIButton!
    @IBOutlet weak var addBoolButton: UIButton!
    
    var customData: [String: Any]? = [:]
    var mainTitle: String?
    
    var dataType: Int = 0
    var dataTitle: String = ""
    
    var name: String = ""
    var data: String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        editJsonButton.setImage(UIImage(systemName: "pencil"), for: UIControl.State.normal)
        clearButton.setImage(UIImage(systemName: "trash"), for: UIControl.State.normal)
        addStringButton.setImage(UIImage(systemName: "character"), for: UIControl.State.normal)
        addNumberButton.setImage(UIImage(systemName: "number"), for: UIControl.State.normal)
        addBoolButton.setImage(UIImage(systemName: "0.circle"), for: UIControl.State.normal)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        mainTitleLabel.text = mainTitle

        DispatchQueue.main.async {
            self.metadataTable.reloadData()
        }
    }
     
    @IBAction private func updateButtonPressed(_ sender: Any) {
        playersController?.onCustomMetadataChanged(metadata: customData)
        customEventController?.onCustomEventDataChanged(data: customData)
        self.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func addStringButtonPressed(_ sender: Any) {
        dataType = 1
        self.name = ""
        self.data = ""
        dataTitle = "(String)"
        performSegue(withIdentifier: "EnterDataSeque", sender: nil)
    }
    
    @IBAction func addNumberButtonPressed(_ sender: Any) {
        dataType = 2
        self.name = ""
        self.data = ""
        dataTitle = "(Number)"
        performSegue(withIdentifier: "EnterDataSeque", sender: nil)
    }
    
    @IBAction func addBoolButtonPressed(_ sender: Any) {
        dataType = 3
        self.name = ""
        self.data = ""
        dataTitle = "(Bool)"
        performSegue(withIdentifier: "EnterDataSeque", sender: nil)
    }
    
    @IBAction func clearButtonPressed(_ sender: Any) {
        self.customData = [:]
        DispatchQueue.main.async {
            self.metadataTable.reloadData()
        }
    }
    
    @IBAction func editJsonButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "EnterCustomMetadataJsonSeque", sender: nil)
    }
    
    
    func addDataToCustomMetadata(_ key: String, _ data: Any?) {
        let keyExists = self.customData?[key] != nil
         
        if keyExists{
            print("The key is present in the dictionary")
        } else {
            switch dataType {
            case 1:
                self.customData?[key] = data as! String
            
            case 2:
                self.customData?[key] = NumberFormatter().number(from: data as! String)
                
            case 3:
                self.customData?[key] = Bool(data as! String)
                
            default:
                print("Unsupported data type")
            }
            
        }
    }
    
    func onDataChanged(name: String?, data: String?) {
        if let dt = data, let nm = name {
            self.name = nm
            self.data = dt
            addDataToCustomMetadata(nm, dt)
        }
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is CustomMetadataJsonViewController {
            let vc = segue.destination as? CustomMetadataJsonViewController
            vc?.mainTitle = mainTitle
            vc?.customMetadata = customData
            vc?.customMetadataController = self
        }
        else if segue.destination is EnterMetadataViewController {
            let vc = segue.destination as? EnterMetadataViewController
            vc?.enterDataTitle = "Enter data \(dataTitle)"
            vc?.data = String(data)
            vc?.customMetadataController = self
        }
    }
}

extension CustomMetadataViewController {
    func onCustomMetadataChanged(metadata: [String:Any]?) {
        self.customData = metadata
    }
}

extension CustomMetadataViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customData?.keys.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellRowIdentifier = "CustomMetadataCellIdentifier"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellRowIdentifier)
        
        guard let cMeta = customData else {
            return cell!
        }
        
        var value = ""
        let keys = cMeta.map { $0.key }
        
        let key = keys[indexPath.row]
        let val = cMeta[key]
        
        switch val {
        case is String:
            value = val as! String
        
        case is Int:
            value = String(val as! Int)
            
        case is Float:
            value = String(val as! Float)
            
        case is Bool:
            value = String(val as! Bool)
        
        default:
            value = "non-convertable"
        }
        
        cell?.textLabel?.text = "\(key):  \(value)"
       
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cMeta = customData else {
            return
        }
        
        self.dismiss(animated: true, completion: nil)

        let keys = cMeta.map { $0.key }
        let key = keys[indexPath.row]
        let val = cMeta[key]
        
        self.name = key
        
        switch val {
        case is String:
            self.data = val as! String
            self.dataType = 1
            dataTitle = "(String)"
            performSegue(withIdentifier: "EnterDataSeque", sender: nil)
            
        case is Int:
            self.data = String(val as! Int)
            self.dataType = 2
            dataTitle = "(Number)"
            performSegue(withIdentifier: "EnterDataSeque", sender: nil)
            
        case is Float:
            self.data = String(val as! Float)
            self.dataType = 2
            self.dataTitle = "(Number)"
            performSegue(withIdentifier: "EnterDataSeque", sender: nil)
            
        case is Bool:
            self.data = String(val as! Bool)
            self.dataType = 2
            self.dataTitle = "(Number)"
            performSegue(withIdentifier: "EnterDataSeque", sender: nil)
            
        default:
            return
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let alert = UIAlertController(title: "Remove item", message: "You are about to remove item.", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: { (action: UIAlertAction!) in
                        guard let cMeta = self.customData else {
                            return
                        }
                        
                        let keys = cMeta.map { $0.key }
                        let key = keys[indexPath.row]
                        
                        self.customData?[key] = nil
                        tableView.deleteRows(at: [indexPath], with: .fade)
                    }))
                    alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
                    self.present(alert, animated: true, completion: nil)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view.
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cell.responds(to: #selector(setter: UIView.preservesSuperviewLayoutMargins)) {
            cell.preservesSuperviewLayoutMargins = false
        }
        
        if cell.responds(to: #selector(setter: UIView.layoutMargins)) {
            cell.layoutMargins = UIEdgeInsets.zero
        }
    }
}

