//
//  CustomEventViewController.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 6.2.22..
//

import Foundation
import UIKit

class CustomEventViewController: UIViewController {

    var playersController: PlayersViewController? = nil

    @IBOutlet private weak var customEventTextField: UITextField!
    @IBOutlet private weak var customEventDataTextView: UITextView!
    @IBOutlet weak var eventDataButton: UIButton!
    
    var customEventName:String? = ""
    var customEventData:[String: Any]? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        customEventTextField.text = customEventName
        let formatedMetadata: String? = customEventData?.jsonString(prettify: true)
        customEventDataTextView.text = formatedMetadata ?? ""
    }
        
    
    
    @IBAction func eventDataButtonPressed(_ sender: Any) {
        performSegue(withIdentifier: "EnterEventDataSeque", sender: nil)
    }
    
    
    
    @IBAction private func updateButtonPressed(_ sender: Any) {
        if let eventName = self.customEventName, let dataDictionary = self.customEventData {
            playersController?.onCustomEventChanged(eventName: eventName, eventData: dataDictionary)
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func onCustomEventDataChanged(data: [String: Any]?){
        self.customEventData = data
        let formatedMetadata: String? = customEventData?.jsonString(prettify: true)
        customEventDataTextView.text = formatedMetadata ?? ""
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        if segue.destination is CustomMetadataViewController {
            let vc = segue.destination as? CustomMetadataViewController
            vc?.mainTitle = "Custom Event Data"
            vc?.customData = customEventData
            vc?.customEventController = self
        }
    }
}


extension CustomEventViewController : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        if(textField == customEventTextField){
            self.customEventName = customEventTextField.text
        }
    }
        
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        return true
        
    }
}
