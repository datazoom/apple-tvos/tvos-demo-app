//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BMPSourceType.h>

NS_ASSUME_NONNULL_BEGIN

/**
 Base class for all media sources.

 @note This class acts as an abstract class.
 */
NS_SWIFT_NAME(MediaSource)
__deprecated_msg("Use SourceConfig(url:type:) instead.")
@interface BMPMediaSource : NSObject <NSCopying>

/** The source type. Possible values are defined in BMPSourceType.h. */
@property (nonatomic) BMPSourceType type;
/** The URL of the media source. */
@property (nonatomic, nonnull, strong) NSURL *url;
/// :nodoc:
- (instancetype)init NS_UNAVAILABLE;
/// :nodoc:
+ (instancetype)new NS_UNAVAILABLE;
/// :nodoc:
- (instancetype)initWithType:(BMPSourceType)sourceType url:(NSURL *)url NS_DESIGNATED_INITIALIZER;
@end

NS_ASSUME_NONNULL_END
