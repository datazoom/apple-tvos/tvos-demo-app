//
// Bitmovin Player iOS SDK
// Copyright (C) 2022, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

typedef NS_ENUM(NSInteger, _BMPImaErrorType) {
  /**
   * An unexpected error occurred while loading or playing the ads.
   * This may mean that the SDK wasn't loaded properly.
   */
  _BMPImaErrorTypeAdUnknownErrorType = 0,
  /**
   * An error occurred while loading the ads.
   */
  _BMPImaErrorTypeAdLoadingFailed = 1,
  /**
   * An error occurred while playing the ads.
   */
  _BMPImaErrorTypeAdPlayingFailed = 2,
};
