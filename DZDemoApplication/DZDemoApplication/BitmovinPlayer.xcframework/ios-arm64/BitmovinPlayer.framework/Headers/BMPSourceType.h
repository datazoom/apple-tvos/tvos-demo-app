//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>

/**
 * Types of media which can be handled by the Bitmovin Player.
 */
typedef NS_ENUM(NSInteger, BMPSourceType) {
    /** indicates a missing source type. */
    BMPSourceTypeNone = 0,
    /** Indicates media of type HLS. */
    BMPSourceTypeHls,
    /** Indicates media of type DASH. */
    BMPSourceTypeDash,
    /** Indicates media of type Progressive MP4. */
    BMPSourceTypeProgressive,
    /**
    Indicates already downloaded media in Apple's `movpkg` format.

    This type could be used to playback a locally stored asset.
    E.g. an asset that was not downloaded using our `OfflineManager`.

    The `url` provided to the `SourceConfig` has to be a file url starting with `file://`.
    Serving a `movpkg` via HTTP is not supported. If a HTTP url (starting with `http(s)://`)
    is provided the initializer will fail and throw an exception.

    AirPlay of a movpkg is supported when the device has an active network connection.
    Limitations:
    - Casting a source of type `movpkg` is not supported.
    - Playback of a partially downloaded `movpkg`, e.g. playback while still downloading, is not supported.
    */
    BMPSourceTypeMovpkg
} NS_SWIFT_NAME(SourceType);
