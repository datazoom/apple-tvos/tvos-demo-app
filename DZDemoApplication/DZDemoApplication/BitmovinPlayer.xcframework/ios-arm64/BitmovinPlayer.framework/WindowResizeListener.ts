import { PlayerEventCallback, PlayerResizedEvent } from "bitmovin-player";
import { PlayerEventV8 } from "./v8/PlayerV8Enums";

export class WindowResizeListener {
  private eventHandler: PlayerEventCallback[] = [];
  private resizeTimer: any;

  constructor() {
    this.setupEventListener();
  }

  onResize(callback: PlayerEventCallback) {
    this.eventHandler.push(callback);
  }

  private setupEventListener() {
    window.addEventListener('resize', () => {
      clearTimeout(this.resizeTimer);
      this.resizeTimer = setTimeout(() => {
        const onPlayerResizeEvent: PlayerResizedEvent = {
          timestamp: 0,
          width: window.innerWidth + 'px',
          height: window.innerHeight + 'px',
          type: PlayerEventV8.PlayerResized as any
        };

        this.eventHandler.forEach((callback) => {
          callback(onPlayerResizeEvent);
        });
      }, 100);
    });
  }
}
