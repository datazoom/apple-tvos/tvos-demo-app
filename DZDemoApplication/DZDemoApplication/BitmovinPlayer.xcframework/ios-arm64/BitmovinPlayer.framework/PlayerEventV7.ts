export enum PlayerEventV7 {
  fullscreenEnter = 'onFullscreenEnter',
  fullscreenExit = 'onFullscreenExit'
}