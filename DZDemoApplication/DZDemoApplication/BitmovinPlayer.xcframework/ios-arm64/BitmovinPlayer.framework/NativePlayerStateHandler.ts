
export interface NativePlayerState {
  currentTime: number,
  isReady: boolean,
  isFullscreen: boolean,
  volume: number,
}

export class NativePlayerStateHandler {

  private nativePlayerState: NativePlayerState = {
    currentTime: 0,
    isReady: false,
    isFullscreen: false,
    volume: 100,
  };

  constructor() {
    this.initAppState();
  }

  get state(): NativePlayerState {
    return this.nativePlayerState;
  }

  reset(): void {
    this.initAppState();
  }

  private initAppState(): void {
    this.nativePlayerState.currentTime = 0;
    this.nativePlayerState.isReady = false;
  }
}
