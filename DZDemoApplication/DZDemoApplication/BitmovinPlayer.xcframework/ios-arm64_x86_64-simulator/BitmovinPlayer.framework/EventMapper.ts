import { EventEnumMapper } from './EventEnumMapper';
import { PlayerEvent, PlayerEventBase, ViewModeChangedEvent } from 'bitmovin-player';
import { PlayerEventV7 } from "../v7/PlayerEventV7";
import { ViewMode } from "../v8/PlayerV8Enums";

export class EventMapper {

  static mapEventToV8(event: PlayerEventBase): PlayerEventBase {
    let mappedEvent = event;
    const eventTypeString = event.type as string;

    switch (eventTypeString) {
      case PlayerEventV7.fullscreenEnter:
      case PlayerEventV7.fullscreenExit:
        mappedEvent = this.mapFullscreenEvent(event);
    }

    mappedEvent.type = EventEnumMapper.mapToV8(event.type) as PlayerEvent;
    return mappedEvent as PlayerEventBase;
  }

  private static mapFullscreenEvent(event: PlayerEventBase): ViewModeChangedEvent {
    const eventTypeString = event.type as string;
    return {
      ...event,
      from: eventTypeString === PlayerEventV7.fullscreenEnter ? ViewMode.Inline : ViewMode.Fullscreen,
      to: eventTypeString === PlayerEventV7.fullscreenExit ? ViewMode.Fullscreen : ViewMode.Inline,
      legacy: false,
    } as ViewModeChangedEvent;
  }
}
