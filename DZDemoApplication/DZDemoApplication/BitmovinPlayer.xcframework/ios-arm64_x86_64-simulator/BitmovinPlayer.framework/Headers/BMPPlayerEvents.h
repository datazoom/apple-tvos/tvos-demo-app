//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#ifndef BMPWebPlayerEvents_h
#define BMPWebPlayerEvents_h

#import <BitmovinPlayer/BMPEvent.h>

@class BMPPlayerEvent;
@class BMPReadyEvent;
@class BMPDestroyEvent;
@class BMPPlayEvent;
@class BMPAudioAddedEvent;
@class BMPAudioChangedEvent;
@class BMPAudioRemovedEvent;
@class BMPDrmDataParsedEvent;
@class BMPDurationChangedEvent;
@class BMPDownloadFinishedEvent;
@class BMPTimeShiftEvent;
@class BMPTimeShiftedEvent;
@class BMPPlaybackFinishedEvent;
@class BMPSeekEvent;
@class BMPSeekedEvent;
@class BMPSourceUnloadedEvent;
@class BMPSourceLoadEvent;
@class BMPSourceLoadedEvent;
@class BMPSourceUnloadEvent;
@class BMPPlayerActiveEvent;
@class BMPPlayerInactiveEvent;
@class BMPPlaylistTransitionEvent;
@class BMPSourceMetadataChangedEvent;
@class BMPPlayerWarningEvent;
@class BMPSourceWarningEvent;
@class BMPPlayerErrorEvent;
@class BMPSourceErrorEvent;
@class BMPAdManifestLoadEvent;
@class BMPAdManifestLoadedEvent;
@class BMPAdErrorEvent;
@class BMPAdQuartileEvent;
@class BMPRenderFirstFrameEvent;
@class BMPStallEndedEvent;
@class BMPStallStartedEvent;
@class BMPPlayingEvent;
@class BMPPausedEvent;
@class BMPMutedEvent;
@class BMPFullscreenDisabledEvent;
@class BMPFullscreenEnabledEvent;
@class BMPFullscreenEnterEvent;
@class BMPFullscreenExitEvent;
@class BMPAirPlayChangedEvent;
@class BMPAirPlayAvailableEvent;
@class BMPMetadataEvent;
@class BMPMetadataParsedEvent;
@class BMPSubtitleRemovedEvent;
@class BMPSubtitleChangedEvent;
@class BMPSubtitleAddedEvent;
@class BMPAdFinishedEvent;
@class BMPAdSkippedEvent;
@class BMPAdStartedEvent;
@class BMPAdClickedEvent;
@class BMPAdBreakFinishedEvent;
@class BMPAdBreakStartedEvent;
@class BMPTimeChangedEvent;
@class BMPVideoDownloadQualityChangedEvent;
@class BMPPlaybackSpeedChangedEvent;
@class BMPCueEvent;
@class BMPCueParsedEvent;
@class BMPCueEnterEvent;
@class BMPCueExitEvent;
@class BMPVideoSizeChangedEvent;
@class BMPUnmutedEvent;
@class BMPCastAvailableEvent;
@class BMPCastPausedEvent;
@class BMPCastPlaybackFinishedEvent;
@class BMPCastPlayingEvent;
@class BMPCastStartEvent;
@class BMPCastStartedEvent;
@class BMPCastStoppedEvent;
@class BMPCastTimeUpdatedEvent;
@class BMPCastWaitingForDeviceEvent;
@class BMPControlsHideEvent;
@class BMPControlsShowEvent;
@class BMPAdScheduledEvent;
@class BMPDvrWindowExceededEvent;

#endif /* BMPWebPlayerEvents_h */
