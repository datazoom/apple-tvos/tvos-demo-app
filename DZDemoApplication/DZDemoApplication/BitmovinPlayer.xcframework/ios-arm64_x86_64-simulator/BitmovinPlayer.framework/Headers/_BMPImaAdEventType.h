//
// Bitmovin Player iOS SDK
// Copyright (C) 2022, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

typedef NS_ENUM(NSInteger, _BMPImaAdEventType) {
  /**
   * Ad break ready.
   */
  _BMPImaAdEventType_AD_BREAK_READY,
  /**
   * Ad break will not play back any ads.
   */
  _BMPImaAdEventType_AD_BREAK_FETCH_ERROR,
  /**
   * Fired the first time each ad break ends. Applications must reenable seeking
   * when this occurs (only used for dynamic ad insertion).
   */
  _BMPImaAdEventType_AD_BREAK_ENDED,
  /**
   * Fired first time each ad break begins playback. If an ad break is watched
   * subsequent times this  will not be fired. Applications must disable seeking
   * when this occurs (only used for dynamic ad insertion).
   */
  _BMPImaAdEventType_AD_BREAK_STARTED,
  /**
   * Fired every time the stream switches from advertising or slate to content.
   * This will be fired even when an ad is played a second time or when seeking
   * into an ad (only used for dynamic ad insertion).
   */
  _BMPImaAdEventType_AD_PERIOD_ENDED,
  /**
   * Fired every time the stream switches from content to advertising or slate.
   * This will be fired even when an ad is played a second time or when seeking
   * into an ad (only used for dynamic ad insertion).
   */
  _BMPImaAdEventType_AD_PERIOD_STARTED,
  /**
   * All valid ads managed by the ads manager have completed or the ad response
   * did not return any valid ads.
   */
  _BMPImaAdEventType_ALL_ADS_COMPLETED,
  /**
   * Ad clicked.
   */
  _BMPImaAdEventType_CLICKED,
  /**
   * Single ad has finished.
   */
  _BMPImaAdEventType_COMPLETE,
  /**
   * Cuepoints changed for VOD stream (only used for dynamic ad insertion).
   * For this event, the <code>IMAAdEvent.adData</code> property contains a list of
   * <code>IMACuepoint</code>s at <code>IMAAdEvent.adData[@"cuepoints"]</code>.
   */
  _BMPImaAdEventType_CUEPOINTS_CHANGED,
  /**
   * The user has closed the icon fallback image dialog. This may be a good time to resume ad
   * playback, which the SDK autopaused on icon tap. This event only fires for tvOS.
   */
  _BMPImaAdEventType_ICON_FALLBACK_IMAGE_CLOSED,
  /**
   * The user has tapped an ad icon. On iOS, the SDK will navigate to the landing page. On tvOS, the
   * SDK will present a modal dialog containing the VAST icon fallback image.
   */
  _BMPImaAdEventType_ICON_TAPPED,
  /**
   * First quartile of a linear ad was reached.
   */
  _BMPImaAdEventType_FIRST_QUARTILE,
  /**
   * An ad was loaded.
   */
  _BMPImaAdEventType_LOADED,
  /**
   * A log event for the ads being played. These are typically non fatal errors.
   */
  _BMPImaAdEventType_LOG,
  /**
   * Midpoint of a linear ad was reached.
   */
  _BMPImaAdEventType_MIDPOINT,
  /**
   * Ad paused.
   */
  _BMPImaAdEventType_PAUSE,
  /**
   * Ad resumed.
   */
  _BMPImaAdEventType_RESUME,
  /**
   * Ad has skipped.
   */
  _BMPImaAdEventType_SKIPPED,
  /**
   * Ad has started.
   */
  _BMPImaAdEventType_STARTED,
  /**
   * Stream request has loaded (only used for dynamic ad insertion).
   */
  _BMPImaAdEventType_STREAM_LOADED,
  /**
   * Stream has started playing (only used for dynamic ad insertion). Start
   * Picture-in-Picture here if applicable.
   */
  _BMPImaAdEventType_STREAM_STARTED,
  /**
   * Ad tapped.
   */
  _BMPImaAdEventType_TAPPED,
  /**
   * Third quartile of a linear ad was reached.
   */
  _BMPImaAdEventType_THIRD_QUARTILE
};
