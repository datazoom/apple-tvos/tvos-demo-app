//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/_BMPJsonEncodable.h>

NS_ASSUME_NONNULL_BEGIN

/**
 * Base class for all config classes.
 */
NS_SWIFT_NAME(Config)
@interface BMPConfig : NSObject <NSCopying, _BMPJsonEncodable>
@end

NS_ASSUME_NONNULL_END
