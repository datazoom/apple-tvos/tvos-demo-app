//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <BitmovinPlayer/BMPSourceConfig.h>

@protocol _BMPOfflineFairplayLicenseStorage;

NS_ASSUME_NONNULL_BEGIN

/**
 * Represents a BMPSourceConfig which references already downloaded or currently downloading offline content. It can be
 * passed to a BMPPlayer instance for playback. Do not create instances of this class on your own, instead
 * use BMPOfflineManager#createOfflineSourceConfigForSourceConfig:restrictedToAssetCache:.
 */
NS_SWIFT_NAME(OfflineSourceConfig)
@interface BMPOfflineSourceConfig : BMPSourceConfig
/** Defines if the player is restricted to the usage of completely offline stored media renditions */
@property (nonatomic, readonly) BOOL isRestrictedToAssetCache;
/// :nodoc:
/// This references either the asset which is used to download offline content or the asset pointing to the already downloaded and stored local asset bundle
@property (nonatomic, strong, readonly) AVURLAsset *_urlAsset;
/// :nodoc:
/// The offline FairPlay storage which contains the content decryption keys needed to properly playback this asset
@property (nonatomic, copy, readonly, nullable) id<_BMPOfflineFairplayLicenseStorage> _fairplayLicenseStorage;

- (instancetype)initWithUrl:(NSURL *)url NS_UNAVAILABLE;
- (instancetype)initWithAdaptiveSource:(BMPAdaptiveSource *)adaptiveSource NS_UNAVAILABLE;
- (instancetype)initWithProgressiveSource:(BMPProgressiveSource *)progressiveSource NS_UNAVAILABLE;
- (instancetype)initWithProgressiveSources:(NSArray<BMPProgressiveSource *> *)progressiveSources NS_UNAVAILABLE;
@end

NS_ASSUME_NONNULL_END
