import { EventMapper } from './EventMapper';
import { EventEnumMapper } from './EventEnumMapper';
import { PlayerEventBase, PlayerEventCallback } from "bitmovin-player";
import { PlayerEventV8 } from "../v8/PlayerV8Enums";

export class EventEmitter {
  private eventHandlers: { [eventType: string]: PlayerEventCallback[]; } = {};

  on(eventType: string, callback: PlayerEventCallback): void {
    if (!this.eventHandlers.hasOwnProperty(eventType)) {
      this.eventHandlers[eventType] = [];
    }

    this.eventHandlers[eventType].push(callback);
  }

  off(eventType: string, callback: PlayerEventCallback): void {
    if (this.eventHandlers.hasOwnProperty(eventType)) {
      this.eventHandlers[eventType] = this.eventHandlers[eventType].filter((element) => {
        return element !== callback;
      });
    }
  }

  fireNativeEvent(eventType: string, dataAsStringOrObject: string | PlayerEventBase): void {
    this.maybeEmitAdditionalEvent(eventType, dataAsStringOrObject);

    const v8EventType = this.maybeMapEventToV8(eventType);

    if (this.eventHandlers.hasOwnProperty(v8EventType) && !this.shouldSuppressEvent(v8EventType)) {
      const event = this.processEvent(v8EventType, eventStringToDataObj(dataAsStringOrObject));
      this.firePlayerEvent(v8EventType, event);
    }
  }

  firePlayerEvent(eventType: string, event: PlayerEventBase) {
    if (this.eventHandlers.hasOwnProperty(eventType)) {
      this.eventHandlers[eventType].forEach((callback) => {
        callback(event);
      });
    }
  }

  private maybeMapEventToV8(eventType: string): PlayerEventV8 {
    // As with UI v3 a few events were dropped we have to map it accordingly.
    // I.e. all event callbacks are stored by their v7 event type string. For events not longer available in v8
    // these events have to be mapped to their possible v7 equivalent. Example: 'onCastPlaying' does no longer exist.
    // Its corresponding v8 event is 'playing'. So onCastPlaying <=> playing <=> onPlaying
    const originalType = eventType;
    eventType = EventEnumMapper.mapEventTypeIfNeeded(eventType);

    if (eventType === originalType) {
      eventType = EventEnumMapper.mapToV8(eventType);
    }

    return eventType as PlayerEventV8;
  }

  private shouldSuppressEvent(eventType: PlayerEventV8): boolean {
    const eventsToSkipInV3: string[] = [
      PlayerEventV8.SourceLoaded,
    ];

    return eventsToSkipInV3.includes(eventType);
  }

  private processEvent(eventType: PlayerEventV8, eventData: PlayerEventBase): PlayerEventBase {
    // In v8 (v3 UI) the onSourceLoaded event is basically the onReady event -> Playback can be started.
    // To not send the onSourceLoaded too early for v8 we need to suppress the actual event.
    if (eventType === PlayerEventV8.Ready) {
      const onSourceLoadedEvent = this.prepareEventData({
        ...eventData,
        type: PlayerEventV8.SourceLoaded as any,
      } as PlayerEventBase);

      this.firePlayerEvent(PlayerEventV8.SourceLoaded, onSourceLoadedEvent);
    }

    return this.prepareEventData(eventData);
  }

  private prepareEventData(data: PlayerEventBase): PlayerEventBase {
    return EventMapper.mapEventToV8(data);
  }

  private maybeEmitAdditionalEvent(eventType: string, dataAsStringOrObject: string | PlayerEventBase) {
    // In v8 (v3 UI) there are no Cast related events anymore. However, as we do not have a `CastPlay` event at all,
    // we simulate a 'CastPlayEvent' in addition to the `CastPlayingEvent` in order to hide the replay overlay.
    // This has to be removed if we ever introduce a `CastPlay` or regular `Play` event for casting.
    if (eventType === 'onCastPlaying') {
      const onPlayEvent = {
        ...eventStringToDataObj(dataAsStringOrObject),
        type: PlayerEventV8.Play as any,
      } as PlayerEventBase;

      this.fireNativeEvent('onCastPlay', onPlayEvent);
    }
  }
}

function eventStringToDataObj(dataAsStringOrObject: string | PlayerEventBase): PlayerEventBase {
  if (typeof dataAsStringOrObject === 'string') {
    return JSON.parse(decodeURIComponent(dataAsStringOrObject));
  }

  return dataAsStringOrObject;
}
