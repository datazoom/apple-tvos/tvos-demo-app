import { BufferType, MediaType, PlayerEventV8, StreamTypeV8, ViewMode, WarningCode } from './v8/PlayerV8Enums';
import {
  AudioQuality,
  AudioTrack,
  BufferLevel,
  DownloadedAudioData,
  DownloadedVideoData,
  LowLatencyAPI,
  PlayerAdvertisingAPI,
  PlayerAPI,
  PlayerBufferAPI,
  PlayerConfig,
  PlayerEvent,
  PlayerEventCallback,
  PlayerExports,
  PlayerSubtitlesAPI,
  PlayerType,
  PlayerVRAPI,
  QueryParameters,
  SegmentMap,
  Snapshot,
  SourceConfig, StreamType,
  SubtitleTrack,
  SupportedTechnologyMode,
  Technology,
  Thumbnail,
  TimeRange,
  VideoQuality,
  ViewModeOptions,
} from 'bitmovin-player';
import { Logger } from './helper/Logger';
import { NativePlayerStateHandler } from './NativePlayerStateHandler';
import { NativeCommunicator } from './helper/NativeCommunicator';
import { EventEmitter } from "./events/EventEmitter";

export class NativePlayerAdapter implements PlayerAPI {

  private stateHandler: NativePlayerStateHandler;
  private playerExports: PlayerExports = {
    PlayerEvent: PlayerEventV8,
    ViewMode,
    WarningCode,
  } as any; // Only enums used by our UI are exported here

  private subtitlesApi: PlayerSubtitlesAPI = {
    list: () => {
      let subtitles = this.getAvailableSubtitleTracks();
      // As the 'off' subtitle gets added in v7 via an unshift() call,
      // we are removing it here again as it will be added through UI v3 anyway
      subtitles.shift();

      const currentSubtitle = this.getSelectedSubtitleTrack();
      return subtitles.map((subtitle: SubtitleTrack) => {
        return {
          ...subtitle,
          enabled: subtitle.id === currentSubtitle.id,
          kind: 'subtitle',
        };
      });
    },
    enable: (id: string) => {
      this.setSubtitle(id);
    },
    disable: (id: string) => {
      // As UI v3 sends 'null' as off id we have to set it explicitly
      this.setSubtitle('off');
    },
    add(subtitle: SubtitleTrack): void {
    },
    remove(subtitleID: string): void {
    },
  };

  private getSelectedAudioTrack(): AudioTrack {
    const audioString = NativeCommunicator.callSynchronously('getAudio');
    if (audioString) {
      return JSON.parse(audioString) as AudioTrack;
    } else {
      return null;
    }
  }

  private getAvailableAudioTracks(): AudioTrack[] {
    const audioTracksString = NativeCommunicator.callSynchronously('getAvailableAudio');
    if (audioTracksString) {
      return JSON.parse(audioTracksString) as [AudioTrack];
    } else {
      return [];
    }
  }

  private getSelectedSubtitleTrack(): SubtitleTrack {
    const subtitleString = NativeCommunicator.callSynchronously('getSubtitle');
    if (subtitleString) {
      return JSON.parse(subtitleString) as SubtitleTrack;
    } else {
      return null;
    }
  }

  private getAvailableSubtitleTracks(): SubtitleTrack[] {
    const subtitleTracksString = NativeCommunicator.callSynchronously('getAvailableSubtitles');
    if (subtitleTracksString) {
      var subtitles = JSON.parse(subtitleTracksString);
      return Object.keys(subtitles).map((key) => {
        return subtitles[key];
      });
    }
    return [];
  }

  private setSubtitle(trackID: string) {
    if (this.getSelectedSubtitleTrack() && trackID === this.getSelectedSubtitleTrack().id) {
      return;
    }
    NativeCommunicator.postEvent('setSubtitle', {'trackID': trackID});
  }

  private bufferApi: PlayerBufferAPI = {
    getLevel: (type: BufferType, media: MediaType): BufferLevel => {
      return {
        level: this.getVideoBufferLength(),
        targetLevel: 0, // Not needed by our UI
        type,
        media,
      };
    },

    setTargetLevel(type: BufferType, value: number, media: MediaType): void {
      Logger.notImplemented('bufferApi.setTargetLevel');
    }
  };

  private eventEmitter: EventEmitter;

  constructor(stateHandler: NativePlayerStateHandler, eventEmitter: EventEmitter) {
    this.eventEmitter = eventEmitter;
    this.stateHandler = stateHandler;
  }

  off(eventType: PlayerEvent, callback: PlayerEventCallback): void {
    this.eventEmitter.off(eventType, callback);
  }

  on(eventType: PlayerEvent, callback: PlayerEventCallback): void {
    this.eventEmitter.on(eventType, callback);
  }

  get exports(): PlayerExports {
    return this.playerExports;
  }

  get subtitles(): PlayerSubtitlesAPI {
    return this.subtitlesApi;
  }

  get buffer(): PlayerBufferAPI {
    return this.bufferApi;
  }

  castStop(): void {
    NativeCommunicator.postEvent('castStop');
  }

  castVideo(): void {
    NativeCommunicator.postEvent('castVideo');
  }

  destroy(): Promise<void> {
    return Promise.resolve();
  }

  getAudio(): AudioTrack {
    return {
      ...this.getSelectedAudioTrack(),
      getQualities: () => {
        Logger.notImplemented('getAvailableAudio.getQualities');
        return [];
      },
    };
  }

  getAvailableAudio(): AudioTrack[] {
    const audioTracks = this.getAvailableAudioTracks();
    return audioTracks.map((track) => {
      return {
        ...track,
        getQualities: () => {
          Logger.notImplemented('getAvailableAudio.getQualities');
          return [];
        },
      };
    });
  }

  getAudioBufferLength(): number {
    // The native player can't distinguish between audio and video so returning the same in both methods is sufficient
    return this.getVideoBufferLength();
  }

  getAudioQuality(): AudioQuality {
    Logger.notImplemented('getAudioQuality');
    // This is needed to prevent a crash in the UI where it want's to access the audio quality even no audio qualities
    // exists.
    return {} as AudioQuality;
  }

  getAvailableAudioQualities(): AudioQuality[] {
    return [];
  }

  getAvailableVideoQualities(): VideoQuality[] {
    return [];
  }

  getConfig(mergedConfig?: boolean): PlayerConfig {
    const configString = NativeCommunicator.callSynchronously('getConfig');
    if (configString) {
      return JSON.parse(configString);
    }
    return {} as PlayerConfig;
  }

  getSource(): SourceConfig | null {
    if (!this.stateHandler.state.isReady) {
      return null;
    }

    const sourceString = NativeCommunicator.callSynchronously('getSource');
    if (sourceString) {
      return JSON.parse(sourceString);
    }
    return {};
  }

  getCurrentTime(): number {
    return this.stateHandler.state.currentTime;
  }

  getDownloadedAudioData(): DownloadedAudioData {
    Logger.notImplemented('getDownloadedAudioData');
    return undefined;
  }

  getDownloadedVideoData(): DownloadedVideoData {
    Logger.notImplemented('getDownloadedVideoData');
    return undefined;
  }

  getDuration(): number {
    const durationString = NativeCommunicator.callSynchronously('getDuration');
    if (durationString) {
      return Number.parseFloat(durationString);
    }
    return 0;
  }

  getContainer(): HTMLElement {
    return document.getElementById('ui-container');
  }

  getMaxTimeShift(): number {
    const maxTimeShiftString = NativeCommunicator.callSynchronously('getMaxTimeShift');
    if (maxTimeShiftString) {
      return Number.parseFloat(maxTimeShiftString);
    }
    return 0;
  }

  getPlaybackSpeed(): number {
    const playbackSpeed = NativeCommunicator.callSynchronously('getPlaybackSpeed');
    if (playbackSpeed) {
      return Number.parseFloat(playbackSpeed);
    }
    return 1;
  }

  getStreamType(): StreamType {
    Logger.notImplemented('getStreamType');
    return StreamTypeV8.Uknown as any;
  }

  getThumbnail(time: number): Thumbnail {
    const thumbnailString = NativeCommunicator.callSynchronously('thumbnailForTime:', time);
    if (thumbnailString) {
      return JSON.parse(thumbnailString);
    }
    return null;
  }

  getTimeShift(): number {
    const timeShiftString = NativeCommunicator.callSynchronously('getTimeShift');
    if (timeShiftString) {
      return Number.parseFloat(timeShiftString);
    }
    return 0;
  }

  getVideoBufferLength(): number {
    const bufferSizeString = NativeCommunicator.callSynchronously('buffer.getLevel', BufferType.ForwardDuration);
    const bufferSize = parseFloat(bufferSizeString);

    return isNaN(bufferSize) ? 0 : bufferSize;
  }

  getVideoQuality(): VideoQuality {
    Logger.notImplemented('getVideoQuality');
    // This is needed to prevent a crash in the UI where it want's to access the video quality even no video qualities
    // exists.
    return {} as VideoQuality;
  }

  getViewMode(): ViewMode {
    if (this.stateHandler.state.isFullscreen) {
      return ViewMode.Fullscreen;
    }

    const isPiPAvailableString = NativeCommunicator.callSynchronously('isPictureInPicture');
    const isPiPInPicture = JSON.parse(isPiPAvailableString) as boolean;

    if (isPiPInPicture) {
      return ViewMode.PictureInPicture;
    }

    return ViewMode.Inline;
  }

  getVolume(): number {
    return this.stateHandler.state.volume;
  }

  hasEnded(): boolean {
    Logger.notImplemented('hasEnded');
    return false;
  }

  isViewModeAvailable(viewMode: ViewMode): boolean {
    if (viewMode === ViewMode.PictureInPicture) {
      const isPiPAvailableString = NativeCommunicator.callSynchronously('isPictureInPictureAvailable');
      const isPiPInPicture = JSON.parse(isPiPAvailableString) as boolean;
      return isPiPInPicture;
    }

    return true;
  }

  isAirplayAvailable(): boolean {
    const isAirPlayAvailableString = NativeCommunicator.callSynchronously('isAirPlayAvailable');
    return JSON.parse(isAirPlayAvailableString);
  }

  isAirplayActive(): boolean {
    const isAirPlayActiveString = NativeCommunicator.callSynchronously('isAirPlayActive');
    return JSON.parse(isAirPlayActiveString);
  }

  showAirplayTargetPicker(): void {
    NativeCommunicator.postEvent('showAirplayTargetPicker');
  }

  isCastAvailable(): boolean {
    const isCastAvailableString = NativeCommunicator.callSynchronously('isCastAvailable');
    return JSON.parse(isCastAvailableString);
  }

  isCasting(): boolean {
    const isCastingString = NativeCommunicator.callSynchronously('isCasting');
    return JSON.parse(isCastingString);
  }

  isLive(): boolean {
    const isLiveString = NativeCommunicator.callSynchronously('isLive');
    return JSON.parse(isLiveString);
  }

  isMuted(): boolean {
    const isMutedString = NativeCommunicator.callSynchronously('isMuted');
    return JSON.parse(isMutedString);
  }

  isPaused(): boolean {
    const isPausedString = NativeCommunicator.callSynchronously('isPaused');
    return JSON.parse(isPausedString);
  }

  isPlaying(): boolean {
    const isPlayingString = NativeCommunicator.callSynchronously('isPlaying');
    return JSON.parse(isPlayingString);
  }

  isStalled(): boolean {
    Logger.notImplemented('isStalled');
    return false;
  }

  mute(): void {
    NativeCommunicator.postEvent('mute');
  }

  pause(): void {
    NativeCommunicator.postEvent('pause');
  }

  play(issuer?: string): Promise<void> {
    return new Promise<void>((resolve) => {
      NativeCommunicator.postEvent('play');
      resolve();
    });
  }

  seek(time: number, issuer?: string): boolean {
    this.stateHandler.state.currentTime = time;
    NativeCommunicator.postEvent('seek', {'time': time});
    return true;
  }

  setAudio(trackID: string): void {
    if (this.getAudio() && trackID === this.getAudio().id) {
      return;
    }

    NativeCommunicator.postEvent('setAudio', {'trackID': trackID});
  }

  setAudioQuality(audioQualityID: string): void {
    Logger.notImplemented('setAudioQuality');
  }

  setPlaybackSpeed(speed: number): void {
    if (!isFinite(speed)) {
      return;
    }
    speed = Number(speed);
    NativeCommunicator.postEvent('setPlaybackSpeed', {'speed': speed});
  }

  setVideoQuality(videoQualityID: string): void {
    Logger.notImplemented('setVideoQuality');
  }

  setViewMode(viewMode: ViewMode, options?: ViewModeOptions): void {
    if (viewMode === ViewMode.PictureInPicture) {
      NativeCommunicator.postEvent('enterPictureInPicture');
      return;
    } else if (viewMode === ViewMode.Fullscreen) {
      NativeCommunicator.postEvent('enterFullscreen');
      return;
    }

    const currentViewMode = this.getViewMode();
    if (currentViewMode === ViewMode.PictureInPicture) {
      NativeCommunicator.postEvent('exitPictureInPicture');
    } else {
      NativeCommunicator.postEvent('exitFullscreen');
    }
  }

  setVolume(volume: number): void {
    if (!isFinite(volume)) {
      return;
    }
    volume = Number(volume);
    if (volume < 0) {
      return;
    }
    this.stateHandler.state.volume = volume;
    NativeCommunicator.postEvent('setVolume', {'volume': volume});
  }

  timeShift(offset: number): void {
    NativeCommunicator.postEvent('timeShift', {'offset': offset});
  }

  unmute(): void {
    NativeCommunicator.postEvent('unmute');
  }

  // Not implemented

  readonly ads: PlayerAdvertisingAPI;
  readonly lowlatency: LowLatencyAPI;
  readonly version: string;
  readonly vr: PlayerVRAPI;

  addMetadata(metadataType: any, metadata: any): boolean {
    Logger.notImplemented('addMetadata');
    return false; // returning false as it's not implemented
  }

  clearQueryParameters(): void {
    Logger.notImplemented('clearQueryParameters');
  }

  getAvailableSegments(): SegmentMap {
    Logger.notImplemented('getAvailableSegments');
    return undefined;
  }

  getBufferedRanges(): TimeRange[] {
    Logger.notImplemented('getBufferedRanges');
    return [];
  }

  getDroppedVideoFrames(): number {
    Logger.notImplemented('getDroppedFrames');
    return 0;
  }

  getManifest(): string {
    Logger.notImplemented('getManifest');
    return undefined;
  }

  getPlaybackAudioData(): AudioQuality {
    Logger.notImplemented('getPlaybackAudioData');
    return undefined;
  }

  getPlaybackVideoData(): VideoQuality {
    Logger.notImplemented('getPlaybackVideoData');
    return undefined;
  }

  getPlayerType(): PlayerType {
    Logger.notImplemented('getPlayerType');
    return undefined;
  }

  getSeekableRange(): TimeRange {
    Logger.notImplemented('getSeekableRange');
    return undefined;
  }

  getSnapshot(type?: string, quality?: number): Snapshot {
    Logger.notImplemented('getSnapshot');
    return undefined;
  }

  getSupportedDRM(): Promise<string[]> {
    Logger.notImplemented('getStreamType');
    return undefined;
  }

  getSupportedTech(mode?: SupportedTechnologyMode): Technology[] {
    Logger.notImplemented('getSupportedTech');
    return undefined;
  }

  getTotalStalledTime(): number {
    Logger.notImplemented('getTotalStalledTime');
    return 0;
  }

  getVideoElement(): HTMLVideoElement | HTMLObjectElement {
    Logger.notImplemented('getVideoElement');
    return undefined;
  }

  isDRMSupported(drmSystem: string): Promise<string> {
    Logger.notImplemented('isDRMSupported');
    return undefined;
  }

  load(source: SourceConfig, forceTechnology?: string, disableSeeking?: boolean): Promise<void> {
    Logger.notImplemented('load');
    return undefined;
  }

  preload(): void {
    Logger.notImplemented('preload');
    return undefined;
  }

  setAuthentication(customData: any): void {
    Logger.notImplemented('setAuthentication');
  }

  setLogLevel(level: any): void {
    Logger.notImplemented('setLogLevel');
  }

  setPosterImage(url: string, keepPersistent: boolean): void {
    Logger.notImplemented('setPosterImage');
  }

  setQueryParameters(queryParameters: QueryParameters): void {
    Logger.notImplemented('setQueryParameters');
    return undefined;
  }

  setVideoElement(videoElement: HTMLElement): void {
    Logger.notImplemented('setVideoElement');
  }

  unload(): Promise<void> {
    Logger.notImplemented('unload');
    return undefined;
  }
}
