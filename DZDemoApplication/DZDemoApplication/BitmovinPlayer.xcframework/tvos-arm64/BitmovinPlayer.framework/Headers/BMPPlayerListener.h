//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BMPPlayerEvents.h>
@class BMPAdBreakStartedEvent;
@class BMPAdBreakFinishedEvent;

@protocol BMPPlayer;

@class BMPAdStartedEvent;
@class BMPAdFinishedEvent;
@class BMPAdSkippedEvent;
@class BMPSourceAddedEvent;
@class BMPSourceRemovedEvent;

NS_ASSUME_NONNULL_BEGIN

/**
 * Defines listener methods for all events available for the Player
 */
NS_SWIFT_NAME(PlayerListener)
@protocol BMPPlayerListener <NSObject>
@optional
/**
 * Called when the player is ready for immediate playback, because initial audio/video has been downloaded.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onReady:(BMPReadyEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player received an intention to start/resume playback.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlay:(BMPPlayEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when playback has been started.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlaying:(BMPPlayingEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player enters the pause state.
 * 
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPaused:(BMPPausedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the current playback time has changed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onTimeChanged:(BMPTimeChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the duration of the current played media has changed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onDurationChanged:(BMPDurationChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player is about to seek to a new position.
 * Only applies to VoD streams.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSeek:(BMPSeekEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when seeking has been finished and data is available to continue playback.
 *
 * Only applies to VoD streams.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSeeked:(BMPSeekedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player is about to time-shift to a new position.
 * Only applies to Live streams.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onTimeShift:(BMPTimeShiftEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when time shifting has been finished and data is available to continue playback. Only applies to live streams, please refer to onSeeked for VoD streams.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onTimeShifted:(BMPTimeShiftedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player is paused or in buffering state and the timeShift offset has exceeded the available timeShift window.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onDvrWindowExceeded:(BMPDvrWindowExceededEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player begins to stall and to buffer due to an empty buffer.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onStallStarted:(BMPStallStartedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player ends stalling, due to enough data in the buffer.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onStallEnded:(BMPStallEndedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the current size of the video content has been changed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onVideoSizeChanged:(BMPVideoSizeChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback of the current media has finished.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlaybackFinished:(BMPPlaybackFinishedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the first frame of the current video is rendered onto the video surface.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onRenderFirstFrame:(BMPRenderFirstFrameEvent *)event player:(id<BMPPlayer>)player __deprecated_msg("As this event is never emitted it will be removed in the future");

/**
 * Called when a player error is encountered.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlayerError:(BMPPlayerErrorEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a source error is encountered.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceError:(BMPSourceErrorEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a player warning occurs.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlayerWarning:(BMPPlayerWarningEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a source warning occurs.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceWarning:(BMPSourceWarningEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when loading of a new source started. This does not mean that loading of the new manifest has been finished.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceLoad:(BMPSourceLoadEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a new source is loaded. This does not mean that loading of the new manifest has been finished.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceLoaded:(BMPSourceLoadedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the current source will be unloaded.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceUnload:(BMPSourceUnloadEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the current source has been unloaded.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceUnloaded:(BMPSourceUnloadedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player was destroyed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onDestroy:(BMPDestroyEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when metadata (i.e. ID3 tags in HLS and EMSG in DASH) are encountered.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onMetadata:(BMPMetadataEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when metadata is parsed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onMetadataParsed:(BMPMetadataParsedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when casting to another device, such as a ChromeCast, is available.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastAvailable:(BMPCastAvailableEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback on a cast device has been paused.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastPaused:(BMPCastPausedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback on a cast device has been finished.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastPlaybackFinished:(BMPCastPlaybackFinishedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when playback on a cast device has been started.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastPlaying:(BMPCastPlayingEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the cast app is launched successfully.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastStarted:(BMPCastStartedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when casting is initiated, but the user still needs to choose which device should be used.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastStart:(BMPCastStartEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when casting to a device is stopped.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastStopped:(BMPCastStoppedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the time update from the currently used cast device is received.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastTimeUpdated:(BMPCastTimeUpdatedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a cast device has been chosen and player is waiting for the device to get ready for playback.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCastWaitingForDevice:(BMPCastWaitingForDeviceEvent *)event player:(id<BMPPlayer>)player __TVOS_PROHIBITED;

/**
 * Called when a sideloaded subtitle cue has been parsed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCueParsed:(BMPCueParsedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a subtitle entry transitions into the active status.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCueEnter:(BMPCueEnterEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when an active subtitle entry transitions into the inactive status.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onCueExit:(BMPCueExitEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a new BMPSubtitleTrack is added, for example using the BMPPlayer#addSubtitle: call.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSubtitleAdded:(BMPSubtitleAddedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when an external BMPSubtitleTrack has been removed so it is possible to update the controls accordingly.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSubtitleRemoved:(BMPSubtitleRemovedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the active BMPSubtitleTrack is changed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSubtitleChanged:(BMPSubtitleChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player is muted.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onMuted:(BMPMutedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player is unmuted.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onUnmuted:(BMPUnmutedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when an audio track is added.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAudioAdded:(BMPAudioAddedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when an audio track is removed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAudioRemoved:(BMPAudioRemovedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the audio track is changed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAudioChanged:(BMPAudioChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback of an ad has been started.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdStarted:(BMPAdStartedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback of an ad has been finished.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdFinished:(BMPAdFinishedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback of an ad has progressed over a quartile boundary.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdQuartile:(BMPAdQuartileEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback of an ad break has been started.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdBreakStarted:(BMPAdBreakStartedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the playback of an ad break has been finished.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdBreakFinished:(BMPAdBreakFinishedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when an ad manifest was successfully downloaded and parsed and the ad has been added onto the queue.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdScheduled:(BMPAdScheduledEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when an ad has been skipped.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdSkipped:(BMPAdSkippedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the user clicks on the ad.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdClicked:(BMPAdClickedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when ad playback fails.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdError:(BMPAdErrorEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the download of an ad manifest is starting.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdManifestLoad:(BMPAdManifestLoadEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the ad manifest has been successfully loaded.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAdManifestLoaded:(BMPAdManifestLoadedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the current video download quality has changed.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onVideoDownloadQualityChanged:(BMPVideoDownloadQualityChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a web download request has finished.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onDownloadFinished:(BMPDownloadFinishedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when AirPlay playback starts or stops.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAirPlayChanged:(BMPAirPlayChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when AirPlay is available.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onAirPlayAvailable:(BMPAirPlayAvailableEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a source config is loaded which has metadata attached or when setSourceMetadata: is called on the player.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceMetadataChanged:(BMPSourceMetadataChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a source was loaded into the Player.
 * Seeking and time shifting is allowed as soon as this event is seen.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlayerActive:(BMPPlayerActiveEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when DRM data is found in a downloaded manifest file.
 * TweaksConfig.nativeHlsParsingEnabled needs to be set to `true` to receive those events.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onDrmDataParsed:(BMPDrmDataParsedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the `Player` was unloaded.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlayerInactive:(BMPPlayerInactiveEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player transitions between two sources in a playlist.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlaylistTransition:(BMPPlaylistTransitionEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when the player transitions from one playback speed to another.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onPlaybackSpeedChanged:(BMPPlaybackSpeedChangedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a new `Source` was added to a playlist.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceAdded:(BMPSourceAddedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called when a `Source` was removed from a playlist.
 *
 * @param event An object holding specific event data.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onSourceRemoved:(BMPSourceRemovedEvent *)event player:(id<BMPPlayer>)player;

/**
 * Called for each occurring player event.
 *
 * @param event The player event. Use `event.name` or [event isKindOfClass:] to check the specific event type.
 * @param player The player instance which is associated with the emitted event
 */
- (void)onEvent:(id <BMPEvent>)event player:(id<BMPPlayer>)player NS_SWIFT_NAME(onEvent(_:player:));
@end

NS_ASSUME_NONNULL_END
