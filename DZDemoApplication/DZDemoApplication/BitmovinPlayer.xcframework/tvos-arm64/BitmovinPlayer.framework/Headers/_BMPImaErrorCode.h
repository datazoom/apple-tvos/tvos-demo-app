//
// Bitmovin Player iOS SDK
// Copyright (C) 2022, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

typedef NS_ENUM(NSInteger, _BMPImaErrorCode) {
  /**
   * The ad response was not recognized as a valid VAST ad.
   */
  _BMPImaErrorCode_VAST_MALFORMED_RESPONSE = 100,
  /**
   * Trafficking error. Video player received an ad type that it was not expecting and/or cannot
   * display.
   */
  _BMPImaErrorCode_VAST_TRAFFICKING_ERROR = 200,
  /**
   * The VAST URI provided, or a VAST URI provided in a subsequent Wrapper
   * element, was either unavailable or reached a timeout, as defined by the
   * video player. The timeout is 8 seconds for initial VAST requests and 4
   * seconds for each subsequent Wrapper.
   */
  _BMPImaErrorCode_VAST_LOAD_TIMEOUT = 301,
  /**
   * The maximum number of VAST wrapper redirects has been reached.
   */
  _BMPImaErrorCode_VAST_TOO_MANY_REDIRECTS = 302,
  /**
   * At least one VAST wrapper loaded and a subsequent wrapper or inline ad
   * load has resulted in a 404 response code.
   */
  _BMPImaErrorCode_VAST_INVALID_URL = 303,
  /**
   *  There was an error playing the video ad.
   */
  _BMPImaErrorCode_VIDEO_PLAY_ERROR = 400,
  /**
   * Failed to load media assets from a VAST response.
   * The default timeout for media loading is 8 seconds.
   */
  _BMPImaErrorCode_VAST_MEDIA_LOAD_TIMEOUT = 402,
  /**
   * Assets were found in the VAST ad response for linear ad, but none of them
   * matched the video player's capabilities.
   */
  _BMPImaErrorCode_VAST_LINEAR_ASSET_MISMATCH = 403,
  /**
   * A companion ad failed to load or render.
   */
  _BMPImaErrorCode_COMPANION_AD_LOADING_FAILED = 603,
  /**
   * An unexpected error occurred and the cause is not known. Refer to the
   * inner error for more information.
   */
  _BMPImaErrorCode_UNKNOWN_ERROR = 900,
  /**
   * Ads list response was malformed.
   */
  _BMPImaErrorCode_PLAYLIST_MALFORMED_RESPONSE = 1004,
  /**
   * There was a problem requesting ads from the server.
   */
  _BMPImaErrorCode_FAILED_TO_REQUEST_ADS = 1005,
  /**
   * Listener for at least one of the required vast events was not added.
   */
  _BMPImaErrorCode_REQUIRED_LISTENERS_NOT_ADDED = 1006,
  /**
   * No assets were found in the VAST ad response.
   */
  _BMPImaErrorCode_VAST_ASSET_NOT_FOUND = 1007,
  /**
   * The ad slot is not visible on the page.
   */
  _BMPImaErrorCode_ADSLOT_NOT_VISIBLE = 1008,
  /**
   * Empty VAST response.
   */
  _BMPImaErrorCode_VAST_EMPTY_RESPONSE = 1009,
  /**
   * There was an error loading the ad.
   */
  _BMPImaErrorCode_FAILED_LOADING_AD = 1010,
  /**
   * There was an error initializing the stream.
   */
  _BMPImaErrorCode_STREAM_INITIALIZATION_FAILED = 1020,
  /**
   * Invalid arguments were provided to SDK methods.
   */
  _BMPImaErrorCode_INVALID_ARGUMENTS = 1101,
  /**
   * Generic invalid usage of the API.
   */
  _BMPImaErrorCode_API_ERROR = 1102,
  /**
   * The version of the runtime is too old.
   */
  _BMPImaErrorCode_OS_RUNTIME_TOO_OLD = 1103,
  /**
   * Another VideoAdsManager is still using the video. It must be unloaded
   * before another ad can play on the same element.
   */
  _BMPImaErrorCode_VIDEO_ELEMENT_USED = 1201,
  /**
   * A video element was not specified where it was required.
   */
  _BMPImaErrorCode_VIDEO_ELEMENT_REQUIRED = 1202,
  /**
   * Content playhead was not passed in, but list of ads has been returned
   * from the server.
   */
  _BMPImaErrorCode_CONTENT_PLAYHEAD_MISSING = 1205,
};
