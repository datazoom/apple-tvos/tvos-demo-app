//
// Bitmovin Player iOS SDK
// Copyright (C) 2017, Bitmovin GmbH, All Rights Reserved
//
// This source code and its use and distribution, is subject to the terms
// and conditions of the applicable license agreement.
//

#import <Foundation/Foundation.h>
#import <BitmovinPlayer/BMPAdItem.h>
#import <BitmovinPlayer/BMPConfig.h>

@class BMPImaAdvertisingConfig;
@class IMAAdsManager;
@class IMASettings;

NS_ASSUME_NONNULL_BEGIN

/**
 * Contains config values regarding the ads which should be played back by the player.
 */
NS_SWIFT_NAME(AdvertisingConfig)
@interface BMPAdvertisingConfig : BMPConfig

/**
 * Contains one or more AdItem(s). Each AdItem defines when the specified ad shall be played.
 *
 * @see BMPAdItem
 */
@property (nonatomic, readonly, copy) NSArray<BMPAdItem *> *schedule;
/**
 * Provides access to the current IMAAdsManager once it becomes available.
 */
@property (nonatomic, copy, nullable) void (^onAdsManagerAvailable)(IMAAdsManager *adsManager);
/**
 * Callback that provides access to the IMASettings before any initialization happens.
 *
 * @discussion Changes to enableDebugMode, playerType and autoPlayAdBreaks will be ignored.
 */
@property (nonatomic, copy, nullable) void (^beforeInitialization)(IMASettings *settings);

/**
 * Configuration to customize Google IMA SDK integration behavior.
 */
@property (nonatomic, readwrite) BMPImaAdvertisingConfig *ima;
/**
 * Creates a new AdvertisingConfig with the given ad schedule.
 *
 * @param schedule The ad schedule which will be used within this AdvertisingConfig.
 * @return The newly created AdvertisingConfig.
 */
- (instancetype)initWithSchedule:(NSArray<BMPAdItem *> *)schedule;

@end

NS_ASSUME_NONNULL_END
