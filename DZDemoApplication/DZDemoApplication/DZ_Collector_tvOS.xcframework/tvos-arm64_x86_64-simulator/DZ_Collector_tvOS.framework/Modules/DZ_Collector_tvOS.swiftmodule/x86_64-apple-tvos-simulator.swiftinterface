// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.6 (swiftlang-5.6.0.323.62 clang-1316.0.20.8)
// swift-module-flags: -target x86_64-apple-tvos14.0-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name DZ_Collector_tvOS
import AVFoundation
import AVKit
import AdSupport
import AppTrackingTransparency
import Foundation
import MobileCoreServices
import Swift
import SystemConfiguration
import UIKit
import UniformTypeIdentifiers
import _Concurrency
import os
public struct DZEventError {
  public var code: Swift.String
  public var message: Swift.String
}
extension DZ_Collector_tvOS.DZEventError : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZConfigEventTypes {
  public var typesContent: [DZ_Collector_tvOS.DZConfigTypeElement]?
  public var typesAd: [DZ_Collector_tvOS.DZConfigTypeElement]?
}
extension DZ_Collector_tvOS.DZConfigEventTypes : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZEventHeader {
  public mutating func initFromConfiguration(_ config: DZ_Collector_tvOS.DZConfig?)
}
extension DZ_Collector_tvOS.DZEventHeader : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZEvent {
}
extension DZ_Collector_tvOS.DZEvent {
  public init(_ type: DZ_Collector_tvOS.EventType)
}
extension DZ_Collector_tvOS.DZEvent : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEvent {
  public mutating func initWithConfigurations(config: DZ_Collector_tvOS.DZConfig, network: DZ_Collector_tvOS.DZConfigNetworkDetails, meta: [Swift.String], flux: [Swift.String])
  public mutating func initWithType(type: DZ_Collector_tvOS.EventType, mediaType: DZ_Collector_tvOS.VideoType)
}
public struct DZServerTimeOffset {
  public let epochMillis: Swift.Int?
}
extension DZ_Collector_tvOS.DZServerTimeOffset : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public protocol DZNativeCollectorProtocol {
  func initPlayer(url: Swift.String, _ completion: ((Swift.String?, AVFoundation.AVPlayer?, Swift.Error?) -> Swift.Void)?)
  func initPlayer(player: AVFoundation.AVPlayer, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  func initAds(url: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
  func deinitPlayer(_ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
}
@objc @_inheritsConvenienceInitializers public class DZNativeCollectortvOS : DZ_Collector_tvOS.DZBaseCollector {
  @objc deinit
  override public init(_ eventManager: DZ_Collector_tvOS.DZEventManager, _ contentDelegate: DZ_Collector_tvOS.DZContentFrameworkProtocol, _ config: DZ_Collector_tvOS.DZConfig)
}
extension DZ_Collector_tvOS.DZNativeCollectortvOS {
  @objc override dynamic open func observeValue(forKeyPath keyPath: Swift.String?, of object: Any?, change: [Foundation.NSKeyValueChangeKey : Any]?, context: Swift.UnsafeMutableRawPointer?)
}
extension DZ_Collector_tvOS.DZNativeCollectortvOS {
  @objc dynamic public func startCollectingData()
  @objc dynamic public func stopCollectingData()
}
extension DZ_Collector_tvOS.DZNativeCollectortvOS : DZ_Collector_tvOS.DZNativeCollectorProtocol {
  @objc dynamic public func initPlayer(url: Swift.String, _ completion: ((Swift.String?, AVFoundation.AVPlayer?, Swift.Error?) -> Swift.Void)?)
  @objc dynamic public func initPlayer(player: AVFoundation.AVPlayer, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  @objc dynamic public func initAds(url: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
  @objc dynamic public func deinitPlayer(_ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
}
public struct DZEventCDN {
  public var cdn: Swift.String
}
extension DZ_Collector_tvOS.DZEventCDN : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public struct DZEventGeoLocation {
  public mutating func initFromConfiguration(_ net: DZ_Collector_tvOS.DZConfigNetworkDetails?)
}
extension DZ_Collector_tvOS.DZEventGeoLocation : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventGeoLocation {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
extension Swift.Float {
  public func rounded(rule: Foundation.NSDecimalNumber.RoundingMode, scale: Swift.Int) -> Swift.Float
}
public protocol DZFrameworkSessionProtocol {
  func getSessionCustomMetadata() -> [Swift.String : Any]?
  func setSessionCustomMetadata(_ metadata: [Swift.String : Any]?)
  func clearSessionCustomMetadata()
  func getPlayerCustomMetadata(playerContext: Swift.String) -> [Swift.String : Any]?
  func setPlayerCustomMetadata(playerContext: Swift.String, _ metadata: [Swift.String : Any]?)
  func clearPlayerCustomMetadata(playerContext: Swift.String)
}
public protocol DZNativeFrameworkProtocol {
  func initPlayer(url: Swift.String, _ completion: ((Swift.String?, AVFoundation.AVPlayer?, Swift.Error?) -> Swift.Void)?)
  func initPlayer(player: AVFoundation.AVPlayer, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  func initPlayer(controller: AVKit.AVPlayerViewController, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  func initAds(url: Swift.String, playerContext: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
  func deinitPlayer(playerContext: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
  func startCollectingData(playerContext: Swift.String)
  func stopCollectingData(playerContext: Swift.String)
}
@_inheritsConvenienceInitializers public class DZNativeCollector : DZ_Collector_tvOS.DZBaseFramework {
  override public init()
  @objc deinit
}
extension DZ_Collector_tvOS.DZNativeCollector : DZ_Collector_tvOS.DZFrameworkSessionProtocol {
  @objc dynamic public func getSessionCustomMetadata() -> [Swift.String : Any]?
  @objc dynamic public func setSessionCustomMetadata(_ metadata: [Swift.String : Any]?)
  @objc dynamic public func clearSessionCustomMetadata()
  @objc dynamic public func getPlayerCustomMetadata(playerContext: Swift.String) -> [Swift.String : Any]?
  @objc dynamic public func setPlayerCustomMetadata(playerContext: Swift.String, _ metadata: [Swift.String : Any]?)
  @objc dynamic public func clearPlayerCustomMetadata(playerContext: Swift.String)
}
extension DZ_Collector_tvOS.DZNativeCollector : DZ_Collector_tvOS.DZNativeFrameworkProtocol {
  @objc dynamic public func initPlayer(url: Swift.String, _ completion: ((Swift.String?, AVFoundation.AVPlayer?, Swift.Error?) -> Swift.Void)?)
  @objc dynamic public func initPlayer(player: AVFoundation.AVPlayer, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  @objc dynamic public func initPlayer(controller: AVKit.AVPlayerViewController, _ completion: ((Swift.String?, Swift.Error?) -> Swift.Void)?)
  public func initAds(url: Swift.String, playerContext: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
  @objc dynamic public func deinitPlayer(playerContext: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
  @objc dynamic public func startCollectingData(playerContext: Swift.String)
  @objc dynamic public func stopCollectingData(playerContext: Swift.String)
}
public struct DZEventCMCD {
}
extension DZ_Collector_tvOS.DZEventCMCD : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventCMCD {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZEventDetailsAttributes {
}
extension DZ_Collector_tvOS.DZEventDetailsAttributes : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventDetailsAttributes {
  public mutating func initWithConfiguration(_ from: [Swift.String])
  public mutating func initWithType(type: DZ_Collector_tvOS.EventType, mediaType: DZ_Collector_tvOS.VideoType)
}
extension Foundation.URL {
  public func mimeType() -> Swift.String
  public func streamingProtocol() -> Swift.String
  public func streamingType() -> Swift.String
  public var containsImage: Swift.Bool {
    get
  }
  public var containsAudio: Swift.Bool {
    get
  }
  public var containsVideo: Swift.Bool {
    get
  }
}
public enum PlayerState {
  case idle
  case contentError
  case contentRequested
  case contentBuffering
  case contentPlaying
  case contentPaused
  case contentSeeking
  case contentSeeked
  case contentCompleted
  case contentSkipped
  case adBreakStarted
  case adBreakEnded
  case adError
  case adRequested
  case adBuffering
  case adPlaying
  case adPaused
  case adSeeking
  case adSeeked
  case adCompleted
  case adSkipped
  case request
  case buffering
  case playing
  case paused
  case completed
  public var state: Swift.String {
    get
  }
}
extension DZ_Collector_tvOS.PlayerState : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector_tvOS.PlayerState : Swift.Identifiable {
  public var id: DZ_Collector_tvOS.PlayerState {
    get
  }
  public typealias ID = DZ_Collector_tvOS.PlayerState
}
extension DZ_Collector_tvOS.PlayerState : Swift.Equatable {
  public static func == (lhs: DZ_Collector_tvOS.PlayerState, rhs: DZ_Collector_tvOS.PlayerState) -> Swift.Bool
}
@_hasMissingDesignatedInitializers public class DZUtility {
  public static func askTrackingConsent()
  public static func getIDFA() -> Swift.String?
  public static func formatEventId(_ eCount: Swift.Int) -> Swift.String
  public static func getIPAddress() -> Swift.String?
  public static func getIPAddressV2() -> Swift.String?
  public static func getIFAddresses() -> Swift.String?
  @objc deinit
}
public protocol DZContentSessionProtocol {
  func onCreateContentSessionID()
  func onResetContentSessionID()
}
public protocol DZSendMessageProtocol {
  func sendMessage(_ data: Foundation.Data)
}
@objc public class DZBaseCollector : ObjectiveC.NSObject {
  public var playerMetadata: [Swift.String : Any]?
  public var sessionMetadata: [Swift.String : Any]?
  public init(_ eventManager: DZ_Collector_tvOS.DZEventManager, _ contentDelegate: DZ_Collector_tvOS.DZContentFrameworkProtocol, _ config: DZ_Collector_tvOS.DZConfig)
  @objc deinit
}
extension DZ_Collector_tvOS.DZBaseCollector {
  public func createRequestId() -> Swift.String
  public func clearRequestId()
  public func createContentSessionId()
  public func resetContenSessionId()
}
extension DZ_Collector_tvOS.DZBaseCollector : DZ_Collector_tvOS.DZContentSessionProtocol {
  public func onCreateContentSessionID()
  public func onResetContentSessionID()
}
extension DZ_Collector_tvOS.DZBaseCollector : DZ_Collector_tvOS.DZPlayerCustomMetadataProtocol {
  public func getPlayerCustomMetadata() -> [Swift.String : Any]?
  public func setPlayerCustomMetadata(_ metadata: [Swift.String : Any]?)
  public func clearPlayerCustomMetadata()
}
public struct DZEventNetworkDetails {
  public var asn: Swift.String
  public var org: Swift.String
  public var isp: Swift.String
  public var connectionType: Swift.String
  public mutating func initFromConfiguration(_ net: DZ_Collector_tvOS.DZConfigNetworkDetails?)
}
extension DZ_Collector_tvOS.DZEventNetworkDetails : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventNetworkDetails {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public enum EventType {
  case none
  case networkTypeChange
  case networkTimed
  case datazoomLoaded
  case error
  case playerReady
  case milestone
  case heartbeat
  case volumeChange
  case mute
  case unmute
  case mediaRequest
  case bufferStart
  case bufferEnd
  case stallStart
  case stallEnd
  case mediaLoaded
  case contentLoaded
  case seekStart
  case seekEnd
  case play
  case playing
  case pause
  case resume
  case resize
  case fullscreen
  case exitFullscreen
  case playbackReady
  case playbackStart
  case playbackComplete
  case renditionChange
  case audioTrackChanged
  case qualityChangeRequest
  case subtitleChange
  case castStart
  case castEnd
  case playBtn
  case adSkipped
  case adBrakeStart
  case adBreakEnd
  case adImpression
  case adClick
  case custom(_: Swift.String, _: [Swift.String : Any]?)
  public var key: Swift.String {
    get
  }
}
extension DZ_Collector_tvOS.EventType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector_tvOS.EventType : Swift.Identifiable {
  public var id: DZ_Collector_tvOS.EventType {
    get
  }
  public typealias ID = DZ_Collector_tvOS.EventType
}
extension DZ_Collector_tvOS.EventType : Swift.Equatable {
  public static func == (lhs: DZ_Collector_tvOS.EventType, rhs: DZ_Collector_tvOS.EventType) -> Swift.Bool
}
public struct DZEventSampling {
  public var samplingRate: Swift.Double
  public var inSample: Swift.Bool
}
extension DZ_Collector_tvOS.DZEventSampling : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public enum AdEventType {
  case none
  case adError
  case adBreakStart
  case adBreakEnd
  case adRequest
  case adLoaded
  case adComplete
  case adImpression
  case adSkipped
  case adClick
  case adPlay
  case adPlaying
  case adPause
  case adResume
  case adPlaybackReady
  case adPlaybackStart
  case adPlaybackComplete
  case adMilestone
  case adRenditionChange
  public var key: Swift.String {
    get
  }
}
extension DZ_Collector_tvOS.AdEventType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector_tvOS.AdEventType : Swift.Identifiable {
  public var id: DZ_Collector_tvOS.AdEventType {
    get
  }
  public typealias ID = DZ_Collector_tvOS.AdEventType
}
extension DZ_Collector_tvOS.AdEventType : Swift.Equatable {
  public static func == (lhs: DZ_Collector_tvOS.AdEventType, rhs: DZ_Collector_tvOS.AdEventType) -> Swift.Bool
}
extension Swift.String {
  public func base64Encode() -> Swift.String
}
public struct DZService {
}
public struct DZConfigTypeElement {
  public let name: Swift.String?
}
extension DZ_Collector_tvOS.DZConfigTypeElement : Swift.Decodable {
  public init(_ name: Swift.String)
  public init(from decoder: Swift.Decoder) throws
}
public struct DZConfigTypeElementV3 {
  public let name: Swift.String?
  public let mediaTypes: [Swift.String]?
}
extension DZ_Collector_tvOS.DZConfigTypeElementV3 : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZEventUserDetails {
}
extension DZ_Collector_tvOS.DZEventUserDetails : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventUserDetails {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
extension Foundation.UserDefaults {
  public enum UserDefaultsKeys : Swift.String {
    case trackingAuthorization
    case trackingAuthorizationOptOut
    case appSessionID
    case appSessionViewID
    case appAdSessionID
    case appSessionStartTime
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public func setTrackingAuthorization(value: Swift.Bool)
  public func isTrackingAuthorized() -> Swift.Bool
  public func isTrackingAuthorizationOptOut() -> Swift.Bool
  @objc dynamic public func createAppSessionID()
  @objc dynamic public func setAppSessionID(uuid: Swift.String)
  @objc dynamic public func clearAppSessionID()
  public func getAppSessionID() -> Swift.String
  @objc dynamic public func setAppSessionStartTime(timestamp: Swift.Double)
  @objc dynamic public func clearAppSessionStartTime()
  public func getAppSessionStartTime() -> Swift.Double
  public func createAppSessionViewID()
  public func setAppSessionViewID(uuid: Swift.String)
  public func clearAppSessionViewID()
  public func getAppSessionViewID() -> Swift.String
  public func createUserAdSessionID()
  public func setUserAdSessionID(adId: Swift.String)
  public func clearUserAdSessionID()
  public func getUserAdSessionID() -> Swift.String
}
public struct DZEventPage {
  public var dzSdkVer: Swift.String
  public var appName: Swift.String
  public init()
}
extension DZ_Collector_tvOS.DZEventPage : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventPage {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZEventDevice {
  public init()
}
extension DZ_Collector_tvOS.DZEventDevice : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventDevice {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZConfigDataCollector {
  public let url: Swift.String?
  public let port: Swift.Int?
}
extension DZ_Collector_tvOS.DZConfigDataCollector : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct JSONCodingKeys : Swift.CodingKey {
  public var stringValue: Swift.String
  public var intValue: Swift.Int?
  public init?(stringValue: Swift.String)
  public init?(intValue: Swift.Int)
}
extension Swift.KeyedDecodingContainer {
  public func decode(_ type: [Swift.String : Any].Type, forKey key: K) throws -> [Swift.String : Any]
  public func decode(_ type: [[Swift.String : Any]].Type, forKey key: K) throws -> [[Swift.String : Any]]
  public func decodeIfPresent(_ type: [Swift.String : Any].Type, forKey key: K) throws -> [Swift.String : Any]?
  public func decode(_ type: [Any].Type, forKey key: K) throws -> [Any]
  public func decodeIfPresent(_ type: [Any].Type, forKey key: K) throws -> [Any]?
  public func decode(_ type: [Swift.String : Any].Type) throws -> [Swift.String : Any]
}
extension Swift.UnkeyedDecodingContainer {
  public mutating func decode(_ type: [Any].Type) throws -> [Any]
  public mutating func decode(_ type: [Swift.String : Any].Type) throws -> [Swift.String : Any]
}
extension Swift.KeyedEncodingContainer {
  public mutating func encodeIfPresent(_ value: [Swift.String : Any]?, forKey key: Swift.KeyedEncodingContainer<K>.Key) throws
  public mutating func encodeIfPresent(_ value: [Any]?, forKey key: Swift.KeyedEncodingContainer<K>.Key) throws
}
extension Swift.UnkeyedEncodingContainer {
  public mutating func encode(contentsOf sequence: [[Swift.String : Any]]) throws
  public mutating func encodeIfPresent(_ value: [Swift.String : Any]) throws
}
extension Swift.Decodable {
  public init?(dictionary: [Swift.String : Any])
}
extension Swift.Encodable {
  public var dictionary: [Swift.String : Any]? {
    get
  }
  public var prettyJSON: Swift.String {
    get
  }
}
@_hasMissingDesignatedInitializers public class JSONUtility {
  @objc deinit
}
public struct DZConfig {
  public let configurationId: Swift.String?
  public let customerCode: Swift.String?
  public let dzApp: Swift.String?
  public let broker: DZ_Collector_tvOS.DZConfigBroker?
  public let dataCollector: DZ_Collector_tvOS.DZConfigDataCollector?
  public let connectorList: Swift.String?
  public let oauthToken: Swift.String?
  public let events: DZ_Collector_tvOS.DZConfigEvents?
  public let eventsV3: DZ_Collector_tvOS.DZConfigEventTypes?
  public let metaDataList: [Swift.String]?
  public let fluxDataList: [Swift.String]?
  public let interval: Swift.Int?
}
extension DZ_Collector_tvOS.DZConfig : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZConfigEvents {
  public let types: [DZ_Collector_tvOS.DZConfigTypeElement]?
  public let metadata: [Swift.String]?
  public let fluxdata: [Swift.String]?
  public let interval: Swift.Int?
}
extension DZ_Collector_tvOS.DZConfigEvents : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
extension Foundation.Data {
  public func toString() -> Swift.String
}
extension UIKit.UIImageView {
  @_Concurrency.MainActor(unsafe) public func downloadedFrom(url: Foundation.URL, contentMode mode: UIKit.UIView.ContentMode = .scaleAspectFit)
  @_Concurrency.MainActor(unsafe) public func downloadedFrom(link: Swift.String, contentMode mode: UIKit.UIView.ContentMode = .scaleAspectFit)
}
extension Foundation.Date {
  public func getTimestamp() -> Swift.UInt64
}
extension Swift.String {
  public func toData() -> Swift.String
}
public struct DZEventCustomMetadata {
  public var sessionMetadata: [Swift.String : Any]
  public var playerMetadata: [Swift.String : Any]
  public var customMetadata: [Swift.String : Any]
}
extension DZ_Collector_tvOS.DZEventCustomMetadata : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventCustomMetadata {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZEventAdData {
  public var adSessionId: Swift.String
  public var adBreakId: Swift.String
  public var adId: Swift.String
  public var adPosition: Swift.Int
  public var adSystem: Swift.String
  public var adWrapperSystem: [Swift.String]
}
extension DZ_Collector_tvOS.DZEventAdData : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventAdData {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
extension Swift.Double {
  @inlinable public func signum() -> Swift.Double {
        if self < 0 { return -1 }
        if self > 0 { return 1 }
        return 0
    }
}
public struct DZEventDetailsMetrics {
}
extension DZ_Collector_tvOS.DZEventDetailsMetrics : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventDetailsMetrics {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public class DZBroker {
  public init(_ base: Swift.String)
  @objc deinit
}
extension UIKit.UIDevice {
  @_Concurrency.MainActor(unsafe) public var modelName: Swift.String {
    get
  }
}
public enum ReachabilityError : Swift.Error {
  case failedToCreateWithAddress(Darwin.sockaddr, Swift.Int32)
  case failedToCreateWithHostname(Swift.String, Swift.Int32)
  case unableToSetCallback(Swift.Int32)
  case unableToSetDispatchQueue(Swift.Int32)
  case unableToGetFlags(Swift.Int32)
}
@available(*, unavailable, renamed: "Notification.Name.reachabilityChanged")
public let ReachabilityChangedNotification: Foundation.NSNotification.Name
extension Foundation.NSNotification.Name {
  public static let reachabilityChanged: Foundation.Notification.Name
}
public class Reachability {
  public typealias NetworkReachable = (DZ_Collector_tvOS.Reachability) -> ()
  public typealias NetworkUnreachable = (DZ_Collector_tvOS.Reachability) -> ()
  @available(*, unavailable, renamed: "Connection")
  public enum NetworkStatus : Swift.CustomStringConvertible {
    case notReachable, reachableViaWiFi, reachableViaWWAN
    public var description: Swift.String {
      get
    }
    public static func == (a: DZ_Collector_tvOS.Reachability.NetworkStatus, b: DZ_Collector_tvOS.Reachability.NetworkStatus) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public enum Connection : Swift.CustomStringConvertible {
    case unavailable, wifi, cellular
    public var description: Swift.String {
      get
    }
    @available(*, deprecated, renamed: "unavailable")
    public static let none: DZ_Collector_tvOS.Reachability.Connection
    public static func == (a: DZ_Collector_tvOS.Reachability.Connection, b: DZ_Collector_tvOS.Reachability.Connection) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public var whenReachable: DZ_Collector_tvOS.Reachability.NetworkReachable?
  public var whenUnreachable: DZ_Collector_tvOS.Reachability.NetworkUnreachable?
  @available(*, deprecated, renamed: "allowsCellularConnection")
  final public let reachableOnWWAN: Swift.Bool
  public var allowsCellularConnection: Swift.Bool
  public var notificationCenter: Foundation.NotificationCenter
  @available(*, deprecated, renamed: "connection.description")
  public var currentReachabilityString: Swift.String {
    get
  }
  @available(*, unavailable, renamed: "connection")
  public var currentReachabilityStatus: DZ_Collector_tvOS.Reachability.Connection {
    get
  }
  public var connection: DZ_Collector_tvOS.Reachability.Connection {
    get
  }
  required public init(reachabilityRef: SystemConfiguration.SCNetworkReachability, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main)
  convenience public init(hostname: Swift.String, queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  convenience public init(queueQoS: Dispatch.DispatchQoS = .default, targetQueue: Dispatch.DispatchQueue? = nil, notificationQueue: Dispatch.DispatchQueue? = .main) throws
  @objc deinit
}
extension DZ_Collector_tvOS.Reachability {
  public func startNotifier() throws
  public func stopNotifier()
  @available(*, deprecated, message: "Please use `connection != .none`")
  public var isReachable: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .cellular`")
  public var isReachableViaWWAN: Swift.Bool {
    get
  }
  @available(*, deprecated, message: "Please use `connection == .wifi`")
  public var isReachableViaWiFi: Swift.Bool {
    get
  }
  public var description: Swift.String {
    get
  }
}
public enum VideoType {
  case none
  case content
  case ad
  public var key: Swift.String {
    get
  }
}
extension DZ_Collector_tvOS.VideoType : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension DZ_Collector_tvOS.VideoType : Swift.Identifiable {
  public var id: DZ_Collector_tvOS.VideoType {
    get
  }
  public typealias ID = DZ_Collector_tvOS.VideoType
}
extension DZ_Collector_tvOS.VideoType : Swift.Equatable {
  public static func == (lhs: DZ_Collector_tvOS.VideoType, rhs: DZ_Collector_tvOS.VideoType) -> Swift.Bool
}
public struct DZEventPlayer {
}
extension DZ_Collector_tvOS.DZEventPlayer : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventPlayer {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZConfigBroker {
}
extension DZ_Collector_tvOS.DZConfigBroker : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public struct DZEventOpsMetadata {
}
extension DZ_Collector_tvOS.DZEventOpsMetadata : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
public class DZEventManager {
  public init(config: DZ_Collector_tvOS.DZConfig, network: DZ_Collector_tvOS.DZConfigNetworkDetails, serverTimeOffset: Swift.Int)
  public func updateNetworkDetails(network: DZ_Collector_tvOS.DZEventNetworkDetails)
  public func updateAppSessionID()
  @objc deinit
}
public struct DZEventVideo {
  public var title: Swift.String
  public var source: Swift.String
  public var mediaType: DZ_Collector_tvOS.VideoType
  public var duration: Swift.Float
  public var frameRate: Swift.Float
  public var playerHeight: Swift.Int
  public var playerWidth: Swift.Int
}
extension DZ_Collector_tvOS.DZEventVideo : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventVideo {
  public mutating func initWithConfiguration(_ from: [Swift.String])
}
public struct DZConfigNetworkDetails {
}
extension DZ_Collector_tvOS.DZConfigNetworkDetails : Swift.Decodable {
  public init(from decoder: Swift.Decoder) throws
}
public protocol DZCustomEventProtocol {
  func sendCustomEvent(name: Swift.String, metadata: [Swift.String : Any]?)
}
public protocol DZBaseFrameworkProtocol {
  func initialize(configurationId: Swift.String, url: Swift.String, _ completion: ((Swift.Bool, Swift.Error?) -> Swift.Void)?)
}
public protocol DZContentFrameworkProtocol {
  func updateNumberOfContentRequest()
  func updateNumberOfContentPlays()
  func getNumberOfContentRequests() -> Swift.Int
  func getNumberOfContentPlays() -> Swift.Int
}
public class DZBaseFramework {
  public init()
  @objc deinit
  public static func getName() -> Swift.String
  public static func getVersion() -> Swift.String
  @objc public func initialize(configurationId: Swift.String, url: Swift.String, _ completion: @escaping (Swift.Bool, Swift.Error?) -> Swift.Void)
}
extension DZ_Collector_tvOS.DZBaseFramework : DZ_Collector_tvOS.DZCustomEventProtocol {
  @objc dynamic public func sendCustomEvent(name: Swift.String, metadata: [Swift.String : Any]?)
}
extension DZ_Collector_tvOS.DZBaseFramework : DZ_Collector_tvOS.DZSendMessageProtocol {
  public func sendMessage(_ data: Foundation.Data)
}
extension DZ_Collector_tvOS.DZBaseFramework : DZ_Collector_tvOS.DZContentFrameworkProtocol {
  public func updateNumberOfContentRequest()
  public func updateNumberOfContentPlays()
  public func getNumberOfContentRequests() -> Swift.Int
  public func getNumberOfContentPlays() -> Swift.Int
}
public protocol DZSessionCustomMetadataProtocol {
  func getSessionCustomMetadata() -> [Swift.String : Any]?
  func setSessionCustomMetadata(_ metadata: [Swift.String : Any]?)
  func clearSessionCustomMetadata()
}
public protocol DZPlayerCustomMetadataProtocol {
  func getPlayerCustomMetadata() -> [Swift.String : Any]?
  func setPlayerCustomMetadata(_ metadata: [Swift.String : Any]?)
  func clearPlayerCustomMetadata()
}
public struct DZEventDetails {
}
extension DZ_Collector_tvOS.DZEventDetails {
  public init(_ type: DZ_Collector_tvOS.EventType)
}
extension DZ_Collector_tvOS.DZEventDetails : Swift.Encodable {
  public func encode(to encoder: Swift.Encoder) throws
}
extension DZ_Collector_tvOS.DZEventDetails {
  public mutating func initWithConfiguration(meta: [Swift.String], flux: [Swift.String])
  public mutating func initWithType(type: DZ_Collector_tvOS.EventType, mediaType: DZ_Collector_tvOS.VideoType)
}
extension Foundation.UserDefaults.UserDefaultsKeys : Swift.Equatable {}
extension Foundation.UserDefaults.UserDefaultsKeys : Swift.Hashable {}
extension Foundation.UserDefaults.UserDefaultsKeys : Swift.RawRepresentable {}
@available(*, unavailable, renamed: "Connection")
extension DZ_Collector_tvOS.Reachability.NetworkStatus : Swift.Equatable {}
@available(*, unavailable, renamed: "Connection")
extension DZ_Collector_tvOS.Reachability.NetworkStatus : Swift.Hashable {}
extension DZ_Collector_tvOS.Reachability.Connection : Swift.Equatable {}
extension DZ_Collector_tvOS.Reachability.Connection : Swift.Hashable {}
