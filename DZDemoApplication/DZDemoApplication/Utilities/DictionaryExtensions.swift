//
//  DictionaryExtensions.swift
//  DZDemoApplication
//
//  Created by Vuk on 14.12.21..
//

import Foundation

extension Dictionary {
    func jsonString(prettify: Bool = false) -> String? {
            guard JSONSerialization.isValidJSONObject(self) else { return nil }
            let options = (prettify == true) ? JSONSerialization.WritingOptions.prettyPrinted : JSONSerialization
                .WritingOptions()
            guard let jsonData = try? JSONSerialization.data(withJSONObject: self, options: options) else { return nil }
            return String(data: jsonData, encoding: .utf8)
        }
}
