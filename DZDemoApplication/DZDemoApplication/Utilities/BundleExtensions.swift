//
//  BundleExtensions.swift
//  DZDemoApplication
//
//  Created by Vuk on 16.12.21..
//

import Foundation

extension Bundle {
    var releaseVersionNumber: String? {
        return infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return infoDictionary?["CFBundleVersion"] as? String
    }
    
    var releaseVersionNumberPretty: String {
            return "ver. \(releaseVersionNumber ?? "1.0.0")"
    }
    
    var buildVersionNumberPretty: String {
            return "build \(buildVersionNumber ?? "1.0.0000")"
    }
}
