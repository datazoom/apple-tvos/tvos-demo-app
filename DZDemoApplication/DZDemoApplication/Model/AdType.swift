//
//  AdType.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 27.11.21..
//

import Foundation

enum AdType {
    case none
    case customUrl(_ url: String)
    case singleInlineLinear
    case singleSkippableInline
    case singleRedirectLinear
    case singleRedirectError
    case singleRedirectBroken
    case singleVPAID20Linear
    case singleVPAID20NonLinear
    case singleNonlinearInline
    case VMAPSessionAdRulePreRoll
    case VMAPPreroll
    case VMAPPrerollBumper
    case VMAPPostroll
    case VMAPPostrollBumper
    case VMAPPreMidPostRollsSingleAds
    case VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAd
    case VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAd
    case VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAdBumpers
    case VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAdBumpers
    case VMAPPrerollSingleAdMidRollStandard5AdsEveryR10Seconds
    case SIMIDSurveyPreRoll
    
    static let defaultType: Self = .none
    
    var title: String {
        switch self {
        case .none:
            return "None"
        case .customUrl:
            return "Custom Ad Tag Url"
        case .singleInlineLinear:
            return "Single Inline Linear"
        case .singleSkippableInline:
            return "Single Skippable Inline"
        case .singleRedirectLinear:
            return "Single Redirect Linear"
        case .singleRedirectError:
            return "Single Redirect Error"
        case .singleRedirectBroken:
            return "Single Redirect Broken (Fallback)"
        case .singleVPAID20Linear:
            return "Single VPAID 2.0 Linear"
        case .singleVPAID20NonLinear:
            return "Single VPAID 2.0 Non-Linear"
        case .singleNonlinearInline:
            return "Single Non-linear Inline"
        case .VMAPSessionAdRulePreRoll:
            return "VMAP Session Ad Rule Pre-roll"
        case .VMAPPreroll:
            return "VMAP Pre-roll"
        case .VMAPPrerollBumper:
            return "VMAP Pre-roll + Bumper"
        case .VMAPPostroll:
            return "VMAP Post-roll"
        case .VMAPPostrollBumper:
            return "VMAP Post-roll + Bumper"
        case .VMAPPreMidPostRollsSingleAds:
            return "VMAP Pre-, Mid-, and Post-rolls, Single Ads"
        case .VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAd:
            return "VMAP - Pre-roll Single Ad, Mid-roll Standard Pod with 3 ads, Post-roll Single Ad"
        case .VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAd:
            return "VMAP - Pre-roll Single Ad, Mid-roll Optimized Pod with 3 Ads, Post-roll Single Ad"
        case .VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAdBumpers:
            return "VMAP - Pre-roll Single Ad, Mid-roll Standard Pod with 3 Ads, Post-roll Single Ad (bumpers around all ad breaks)"
        case .VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAdBumpers:
            return "VMAP - Pre-roll Single Ad, Mid-roll Optimized Pod with 3 Ads, Post-roll Single Ad (bumpers around all ad breaks)"
        case .VMAPPrerollSingleAdMidRollStandard5AdsEveryR10Seconds:
            return "VMAP - Pre-roll Single Ad, Mid-roll Standard Pods with 5 Ads Every 10 Seconds for 1:40, Post-roll Single Ad"
        case .SIMIDSurveyPreRoll:
            return "SIMID Survey Pre-roll"
        }
    }
    
    var url: String? {
        switch self {
        case .none:
            return nil
        case .customUrl(let url):
            return url
        case .singleInlineLinear:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinear&correlator="
        case .singleSkippableInline:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dskippablelinear&correlator="
        case .singleRedirectLinear:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dredirectlinear&correlator="
        case .singleRedirectError:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dredirecterror&nofb=1&correlator="
        case .singleRedirectBroken:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dredirecterror&correlator="
        case .singleVPAID20Linear:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dlinearvpaid2js&correlator="
        case .singleVPAID20NonLinear:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dnonlinearvpaid2js&correlator="
        case .singleNonlinearInline:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=480x70&iu=/124319096/external/single_ad_samples&ciu_szs=300x250&impl=s&gdfp_req=1&env=vp&output=vast&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ct%3Dnonlinear&correlator="
        case .VMAPSessionAdRulePreRoll:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&cust_params=sar%3Da0f2&unviewed_position_start=1&tfcd=0&npa=0&correlator="
        case .VMAPPreroll:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpreonly&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPrerollBumper:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpreonlybumper&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPostroll:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpostonly&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPostrollBumper:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpostonlybumper&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPreMidPostRollsSingleAds:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpost&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAd:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostpod&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAd:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostoptimizedpod&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAdBumpers:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostpodbumper&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAdBumpers:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostoptimizedpodbumper&cmsid=496&vid=short_onecue&correlator="
        case .VMAPPrerollSingleAdMidRollStandard5AdsEveryR10Seconds:
            return "https://pubads.g.doubleclick.net/gampad/ads?sz=640x480&iu=/124319096/external/ad_rule_samples&ciu_szs=300x250&ad_rule=1&impl=s&gdfp_req=1&env=vp&output=vmap&unviewed_position_start=1&cust_params=deployment%3Ddevsite%26sample_ar%3Dpremidpostlongpod&cmsid=496&vid=short_tencue&correlator="
        case .SIMIDSurveyPreRoll:
            return "https://pubads.g.doubleclick.net/gampad/ads?iu=/21775744923/external/simid&description_url=https%3A%2F%2Fdevelopers.google.com%2Finteractive-media-ads&tfcd=0&npa=0&sz=640x480&gdfp_req=1&output=vast&unviewed_position_start=1&env=vp&impl=s&correlator="
        }
    }
    
    var shouldEnterCustomUrl: Bool {
        switch self {
        case .customUrl:
            return true
        default:
            return false
        }
    }
    
    static let allCases: [Self] = [.none,.singleInlineLinear,.singleSkippableInline,.singleRedirectLinear,.singleRedirectError,.singleRedirectBroken,.singleVPAID20Linear,.singleVPAID20NonLinear,.singleNonlinearInline,.VMAPSessionAdRulePreRoll,.VMAPPreroll,.VMAPPrerollBumper,.VMAPPostroll,.VMAPPostrollBumper,.VMAPPreMidPostRollsSingleAds,.VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAd,.VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAd,.VMAPPrerollSingleAdMidRollStandard3AdsPostRollSingleAdBumpers,.VMAPPrerollSingleAdMidRollOptimized3AdsPostRollSingleAdBumpers,.VMAPPrerollSingleAdMidRollStandard5AdsEveryR10Seconds,.SIMIDSurveyPreRoll,.customUrl("")]
}



extension AdType : Hashable {
    func hash(into hasher: inout Hasher) {
        hasher.combine(self.title)
    }
}

extension AdType : Identifiable {
    var id: Self { self }
}

extension AdType : Equatable {
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.hashValue == rhs.hashValue
      }
}
