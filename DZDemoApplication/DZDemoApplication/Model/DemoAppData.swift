//
//  DemoAppData.swift
//  DZDemoApplication
//
//  Created by Vuk on 31.5.22..
//

import Foundation
import UIKit
import AVKit
import AVFoundation
import os

#if NATIVE
import DZ_Collector_tvOS
#elseif BITMOVIN
import BitmovinPlayer
import DZ_Collector_iOS
//#elseif AKAMAI
//import DZCollectorTvOSAkamai
//import AmpCore
#endif

#if canImport(GoogleInteractiveMediaAds)
import GoogleInteractiveMediaAds
#endif

#if canImport(AmoAMI)
import AmpAMI
import DZCollector
#endif


public class DemoAppData {
    static let shared = DemoAppData()

    var selectedPlayerId: String?
    var fullscreenFlag: Bool = false

#if NATIVE
    var nativeFramework: DZNativeCollector? = nil
    var players: [String: AVPlayer] = [:]
    
    var controllers: [String: NativePlayerViewController] = [:]

#elseif BITMOVIN
    var bitmovinFramework: DZ_Collector? = nil
    var bitmovinPlayers: [String: Player] = [:]
    
    var controllers: [String: BitmovinPlayerViewController] = [:]

//#elseif AKAMAI
//    var akamaiFramework: DZAkamaiFramework? = nil
//    var akamaiPlayer: AmpPlayer?
#endif
    
    var playerConfiguration : DZPlayerConfiguration = DZPlayerConfiguration()
    
    var customEventName:String = ""
    var customEventData:[String: Any] = [:]
    
    public func initData() {
        self.controllers = [:]

        self.playerConfiguration = DZPlayerConfiguration()
        
        self.customEventName = ""
        self.customEventData = [:]
    }
}
