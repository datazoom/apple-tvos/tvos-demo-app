//
//  DZEnvironment.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 27.11.21..
//

import Foundation

extension String {
    static let developmentURL = "https://devplatform.datazoom.io"
    static let stagingURL = "https://stagingplatform.datazoom.io"
//    static let preProductionURL = "https://demoapi.datazoom.io"
    static let productionURL = "https://platform.datazoom.io"
}

public enum DZEnvironment: Int {
    case development = 0
    case staging = 1
//    case preProduction = 2
    case production = 2
    
    
    var urlString: String {
        switch self {
        case .development:
            return .developmentURL
        case .staging:
            return .stagingURL
//        case .preProduction:
//            return .preProductionURL
        case .production:
            return .productionURL
        }
    }
    
    static var defaultEnvironment: Self {
        return .staging
    }
}
