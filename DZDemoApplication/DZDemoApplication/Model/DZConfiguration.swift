//
//  DZConfiguration.swift
//  DZNativeDemoApplication
//
//  Created by Vuk on 27.11.21..
//

import Foundation

class DZLibraryConfiguration {
    var configurationId: String = ""
    var apiEndpoint: String = ""
    
    init() {
        self.configurationId = ""
        self.apiEndpoint = ""
    }
    
    init(_ configurationId: String, _ apiEndpoint: String) {
        self.configurationId = configurationId
        self.apiEndpoint = apiEndpoint
    }
}

class DZPlayerConfiguration {
    var assetUrl: String
    var assetType: VideoType
    
    var adType: AdType
    var adUrl: String

    init() {
        self.assetUrl = VideoType.defaultType.url
        self.assetType = VideoType.defaultType
        self.adType = AdType.defaultType
        self.adUrl = AdType.defaultType.url ?? ""
    }
    
    init(_ assetUrl: String?, _ assetType: VideoType?, _ adType: AdType?, _ adUrl: String?) {
        self.assetUrl = assetUrl ?? VideoType.defaultType.url
        self.assetType = assetType ?? VideoType.defaultType
        self.adType = adType ?? AdType.defaultType
        self.adUrl = adUrl ?? (AdType.defaultType.url ?? "")
    }
}
